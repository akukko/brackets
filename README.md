# brackets

## Current features:
- Read a text file containing team names and generate evenly spaced groups.
- Generate a schedule for the group games.
- Generate playoff matches based on the group stage results.

## Usage

- The tournament generator can be run with `stack run generator <teamfile>`.

- The parameter teamfile is a path to the teamfile (plain text file containing one team per line).

- Running the program for the first time will generate a tournament based on the amount of teams in the teamfile and settings defined in `src/Config.hs`.
    - The plan for the future is to make the tournament generation more configurable and easier to use.
    - The logic for splitting teams in groups is by default the one used for Ruskakyykkä 2021.
        - The maximum amount of fields on the Kuusikkopuisto big field is 12.
        - Thus the maximum amount of groups is also 12, and the maximum group size 4.
            - Groups of four can be played relatively fast on a single field, five or more teams would take considerably more time.
            - This makes the maximum amount of participating teams 48.
        - If there are less than 48 teams, the group amount is modified so that the available fields are utilized in an efficient manner.
            - Some groups can then play on two fields simultaneously and those groups can accommodate more teams than four without lengthening the group stage.
        - For example, 25 teams would play in five groups, each using two fields to play their games.
            - If a single game takes 30 minutes, this kind of group stage will take 2.5 hours
            - For comparison, playing with four teams on one field takes 3 hours, and the teams get two games less.

- Once the program has been run once, it will generate the necessary save files (in the `data` directory), so next time you run the program, it's enough to run it without any parameters. 
    - Running it without parameters will output the current information in the save files.

- As of right now, adding results for the matches is a bit of a manual process, and involves manually editing the `data/groupStage.txt` and `data/playoffs.txt` files.


Group output

```
Group 1
==================================================================
|    | Team                    | Games | Points | Average | Best |
==================================================================
|  1 | AiM                     |     3 |      6 |   -2.00 |    1 |
|  2 | SuppoHauet              |     3 |      4 |    1.00 |    3 |
|  3 | DDR:n Naisvoimistelijat |     3 |      2 |   -5.00 |   -2 |
|  4 | Skepparklubben          |     3 |      0 |   -6.00 |   -4 |
==================================================================

         Skepparklubben    -8  -   -2  AiM                    
         Skepparklubben    -6  -    2  SuppoHauet
DDR:n Naisvoimistelijat   -10  -   -5  AiM                    
DDR:n Naisvoimistelijat    -2  -    3  SuppoHauet
                    AiM     1  -   -2  SuppoHauet             
         Skepparklubben    -4  -   -3  DDR:n Naisvoimistelijat

```

Playoffs output

```
Semifinals
==========

                    AiM        -       Skepparklubben
             SuppoHauet        -       DDR:n Naisvoimistelijat            

Final
=====

                    TBA        -       TBA            
```
