cabal-version: 3.0

name:           brackets
version:        0.1.0.0
description:    Brackets is a software for generating kyykkä tournaments in a user-specified format.
author:         Antti Kukko
maintainer:     akukko@protonmail.com
copyright:      2023 Antti Kukko
license:        BSD-3-Clause
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md
    ChangeLog.md

common shared
  default-extensions: OverloadedStrings
                    , TupleSections
                    , ImportQualifiedPost
                    , ViewPatterns
                    , RecordWildCards
  ghc-options:
    -- Optimizations
    -- =============

    -- -O
    -- -O2

    -- -funbox-strict-fields
    -- -fexcess-precision

    -- Errors
    -- ======
    -Werror
    -Wincomplete-patterns
    -Wname-shadowing


    -- Plain warnings
    -- ==============

    -W

    -Wwarn=unused-imports
    -- -Wwarn=dodgy-imports
    -- -Wwarn=dodgy-foreign-imports
    -Wwarn=unused-top-binds
    -Wwarn=unused-local-binds
    -Wwarn=unused-matches

    -- Disabled warnings
    -- =================
    -Wno-missing-home-modules
    -Wno-missed-extra-shared-lib

    -fconstraint-solver-iterations=0


  build-depends:
      base >=4.7 && <5
    , base64
    , binary
    , blaze-builder
    , blaze-html
    , blaze-markup
    , bytestring
    , bytestring-encoding
    , concurrent-extra
    , containers
    , criterion
    , criterion-measurement
    , deepseq
    , directory
    , extra
    , hashable
    , http-types
    , mtl
    , pretty-show
    , random-shuffle
    , safe
    , scalpel
    , text
    , text-show
    , time
    , unicode-show
    , unordered-containers
    , utf8-string
    , wai
    , warp



library
  import: shared
  exposed-modules:
      Balance
      Brackets
      Config
      Data.Competition
      Data.Match
      Data.Matchup
      Data.Result
      Data.Score
      Data.TeamScore
      Data.Types
      Data.Vault
      Format
      Groups
      Helpers
      Interactive
      IO.FetchLeagueResults
      IO.Locations
      IO.ReadResults
      IO.ReadTeams
      IO.Write
      League
      Printing
      Scheduling
      Scoreboard
      Time
      Web.Admin
      Web.Difference
      Web.Fantasy
      Web.Front
      Web.Group
      Web.Links
      Web.Results
      Web.Server
  other-modules:
      Paths_brackets
  hs-source-dirs:
      src
  default-language: Haskell2010


executable generator
  import: shared
  main-is: Main.hs
  other-modules:
      Paths_brackets
  hs-source-dirs:
      generator
  ghc-options: -threaded -rtsopts -with-rtsopts=-N

  build-depends: brackets
  default-language: Haskell2010

executable server
  import: shared
  main-is: Main.hs
  other-modules:
      Paths_brackets
  hs-source-dirs:
      server
  ghc-options: -threaded -rtsopts -with-rtsopts=-N

  build-depends: brackets
  default-language: Haskell2010

test-suite brackets-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      Arbitrary
      GeneratorTests
      LeagueTests
      TestHelpers
      WebScrapingTests
      Paths_brackets
  hs-source-dirs:
      test
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      HUnit
    , QuickCheck
    , base >=4.7 && <5
    , base64
    , binary
    , blaze-builder
    , blaze-html
    , blaze-markup
    , brackets
    , bytestring
    , bytestring-encoding
    , concurrent-extra
    , containers
    , criterion
    , criterion-measurement
    , deepseq
    , directory
    , extra
    , hashable
    , http-types
    , mtl
    , pretty-show
    , pretty-terminal
    , random-shuffle
    , safe
    , scalpel
    , text
    , text-show
    , time
    , unicode-show
    , unordered-containers
    , utf8-string
    , wai
    , warp
  default-language: Haskell2010
