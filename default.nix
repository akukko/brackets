let
    # pin the channel to ensure reproducibility
    pkgs = import <nixpkgs> { };
in
pkgs.haskellPackages.developPackage {
    root = ./.;
    modifier = drv:
        pkgs.haskell.lib.addBuildTools drv (with pkgs;
            [ haskellPackages.cabal-install
              haskellPackages.stylish-haskell
              haskellPackages.hoogle
              haskellPackages.haskell-language-server
              ncurses
              zlib
            ]);
}
