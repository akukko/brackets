{-# LANGUAGE OverloadedStrings #-}

module Brackets
    ( getFirstRound
    , getBrackets
    , getSingleEliminationBracket
    , getFirstRoundFromRanked
    ) where


import Scoreboard
import Helpers
import Data.Types
import Data.Match as Match
import Config


import Data.List.NonEmpty as NE (NonEmpty(..))


getSingleEliminationBracket :: [[TeamScore]] -> MatchID -> Bracket
getSingleEliminationBracket ts mid = seriesToBrackets bs []
    where
      bs = map Seeded (getFirstRound ts mid)


--                  original     accumulator  final bracket
seriesToBrackets :: [Bracket] -> [Bracket] -> Bracket
seriesToBrackets (a:b:bs) acc = seriesToBrackets bs (WinnersOf a b : acc)
seriesToBrackets (a:bs) [] = a
seriesToBrackets (a:bs) _ = error "Uneven amount of brackets and there's still something in accumulated."
seriesToBrackets [] acc = seriesToBrackets acc []

getFirstRound :: [[TeamScore]] -> MatchID -> [Match]
getFirstRound = getFirstRoundFromRanked . getRanked

getFirstRoundFromRanked :: [TeamScore] -> MatchID -> [Match]
getFirstRoundFromRanked ts = buildBrackets (map (Just . team) ts)

-- FIXME: make the brackets configurable (best of series)
buildBrackets :: [Maybe Roster] -> MatchID -> [Match]
buildBrackets ts' = recurse top bot'
    where
        recurse ((Just t1):ts) ((Just t2):bs) mid =
            Scheduled playoffMatchFormat
                (Matchup mid t1 t2) : recurse ts bs (incr mid)
        recurse ((Just t1):ts) (Nothing:bs) mid =
            HomeOnly mid t1 : recurse ts bs (incr mid)
        recurse (Nothing:ts) ((Just t2):bs) mid =
            AwayOnly mid t2 : recurse ts bs (incr mid)
        recurse (Nothing:ts) (Nothing:bs) mid =
            Unscheduled mid : recurse ts bs (incr mid)
        recurse [] (x:xs) _ = [] --error $ "What now: " ++ ppShow x
        recurse (x:xs) [] _ = [] --error $ "What now: " ++ ppShow x
        recurse  _ _ _ = []

        (top, bot) = splitHalf ts'
        bot' = reverse bot

getBrackets :: [Match] -> [[Match]]
getBrackets [] = []
getBrackets (r:rs)
    | null winners = []
    | otherwise = (r:rs) : getBrackets br
    where
        winners = map Match.winner (r:rs)
        br = buildBrackets winners mid
        mid = lastMid (r NE.:| rs)
