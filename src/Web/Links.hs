{-# LANGUAGE OverloadedStrings #-}
module Web.Links where

import Config


import Text.Blaze.Html5 as H hiding (map)

import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Utf8
import Control.Monad
import qualified Data.Text as T


--h1Style = "color:#2e2e2e; background-color: powderblue;"
h1Style = "color: " <> grey <> "; background-color: #ff9059 ;"
banner
    = H.code
    $ H.pre
    $ h1 " "
    <> H.toHtml competitionName
    <> " "
    <> H.toHtml competitionSeason
    <> " live scores"
    ! A.style h1Style

grey = "#2e2e2e"

codeBlock x = H.pre $ H.code x

navigation = breadcrumb ["index", "results", "groups"]

breadcrumb pages = do
    let ulStyle = "padding: 10px 16px; list-style: none; \
                  \background-color: #eee"
    codeBlock $ H.ul ! A.style ulStyle $ forM_ pages crumb

crumb url = do
    let liStyle = "display: inline; font-size: 1.4em;"
    let linkColor = "color: black;"
    H.li ! A.style liStyle $ H.a ! A.href (textValue url) !
        A.style linkColor $ toHtml (" / " <> url)


resetUrl :: T.Text -> Html
resetUrl url = do
    let scr = "window.history.replaceState(\
              \{}, document.title, '/' + '" <> url <> "');"

    H.script (toHtml scr)

adminNavigation = breadcrumb ["panel", "admin"]
adminHeadBody = headBodyBuilder adminNavigation

headBody = headBodyBuilder navigation

headBodyBuilder nvg ttl bodyContent =
    renderHtml $ do
        meta ! charset "UTF-8"
        meta ! name "viewport"
             ! content "width=device-width, initial-scale=0.65, user-scalable=yes"
        H.head $ H.title ttl

        let colorScheme = "color:white;background-color:#2e2e2e;"
        body ! A.style colorScheme $ do
            banner
            nvg
            bodyContent

linkPage =
    headBody "Index" $ do ""
