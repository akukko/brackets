{-# LANGUAGE OverloadedStrings #-}
module Web.Server where

import Web.Front
import Web.Fantasy
import Web.Results
import Web.Links
import Web.Group
import Web.Admin
import Config
import Data.Vault

import Data.Maybe
import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types (status200, status401)
import Data.Text as T

import Control.Monad.IO.Class
import Data.ByteString.Lazy (fromStrict)
import Data.ByteString.Encoding
import Data.Competition (updateMatchWithID)
import Data.Types
import Control.Monad.Extra (whenJust)
import qualified Data.Text.Encoding as TE
import Control.Concurrent.Lock
import qualified Data.HashMap.Strict as HashMap
import Text.Read (readMaybe)
import Data.ByteString.Base64
import Data.ByteString (stripPrefix)

runServer = do
    let port = 40136
    putStrLn $ "Listening on port " ++ show port
    dbLock <- liftIO new
    run port (app dbLock)


app :: MonadIO m => Lock -> Request -> (Response -> m b) -> m b
app dbLock req respond = do
    let path = pathInfo req
    let query = queryString req
    let headers = requestHeaders req
    let msg = "Got request (" ++ show path ++ ") (" ++ show query ++ ")"
    liftIO $ putStrLn msg
    x <- liftIO $ handler dbLock path query headers
    respond x


authenticate headers go = do
    let isAuth ("Authorization", _) = True
        isAuth _ = False
    let authOK =
            case Prelude.filter isAuth headers of
                ((_, x):xs) -> do
                    case Data.ByteString.stripPrefix "Basic " x of
                        Just secret ->
                            case decodeBase64 secret of
                                -- FIXME: Next time this is used, get this
                                --        from disk or something like that
                                Right "aina:maksimi" -> True
                                _ -> False
                        Nothing -> False
                _ -> False


    if authOK
        then go
        else return $ authFail ""


-- authenticated pages
handler l ["admin"] [ ("matchId", Just mid)
                    , ("home", Just hs)
                    , ("away", Just as)
                    ] h =
    authenticate h $ do
        saveResult l mid hs as
        resultPage l Rewrite "admin"

handler l ["admin"] _ h = authenticate h $ resultPage l Rewrite "admin"
handler l ["panel"] _ h = authenticate h $ adminPage l
handler l ["regenerate"] q h =
    authenticate h $ do
        (gps, _) <- getMatches l

        let filt ("remove", t) = t
            filt _ = Nothing

        let teamsToRemove = Prelude.map TE.decodeUtf8 $ mapMaybe filt q

        with l $ html <$> regenerateGroups teamsToRemove gps

-- non-authenticated pages
handler l ["ofkl"] _ _ = fantasyPage
handler l ["index"] _ _ = indexPage
handler l ["groups"] _ _ = allGroupsPage l
handler l ["results"] [ ("matchId", Just mid)
                    , ("home", Just hs)
                    , ("away", Just as)
                    ] _ = do
    saveResult l mid hs as
    resultPage l Write "results"
handler l ["results"] query _ = resultPage l Read "results"
handler l [n] [ ("matchId", Just mid)
                         , ("home", Just hs)
                         , ("away", Just as)
                         ] _ = do
    saveResult l mid hs as
    groupPage l n

handler l [n] _ _
    | isJust (readMaybe (T.unpack n) :: Maybe Int) = groupPage l n
handler l _ _ _ = indexPage

saveResult l matchID homeScore awayScore = do
    let mid = read (T.unpack $ TE.decodeUtf8 matchID) :: Int
    let hs = read (T.unpack $ TE.decodeUtf8 homeScore) :: Int
    let as = read (T.unpack $ TE.decodeUtf8 awayScore) :: Int

    with l $ do
        c <- tryGetCompetition Config.competitionId
        let c' = updateMatchWithID (MatchID competitionId mid) (Scores hs as) <$> c
        whenJust c' $ \x -> do
            print "saving competition"
            saveCompetition x



authFail = responseLBS status401
                [ ("Content-Type", "text/html")
                , ("charset", "utf-8")
                , ("WWW-Authenticate", "Basic realm=\"kyykka.pro\"")
                ]
html = responseLBS status200 [("Content-Type", "text/html"), ("charset", "utf-8")]
plain = responseLBS status200 [("Content-Type", "text/plain"), ("charset", "utf-8")]
page p = (plain <$> fromStrict) . encode utf8_bom <$> p


getMatches l =
    with l $ do
        v <- getVault
        let gps = maybe [] groups (HashMap.lookup competitionId (competitions v))
        let poffs = maybe [] playoffs (HashMap.lookup competitionId (competitions v))
        return (gps, poffs)

allGroupsPage l = do
    (gps, _) <- getMatches l
    return $ html $ allGroups gps

adminPage l = do
    (gps, _) <- getMatches l
    return $ html $ adminPanel gps


indexPage :: IO Response
indexPage = return $ html linkPage

groupPage :: Lock -> Text -> IO Response
groupPage l n =
    case readMaybe (T.unpack n) :: Maybe Int of
        Just num -> do
            (gps, _) <- getMatches l
            return $ html $ getGroupPage gps num
        Nothing -> return $ html "404 Not found"

resultPage :: Lock -> Permission -> T.Text -> IO Response
resultPage l permission url = do
    (gps, poffs) <- getMatches l
    return $ html $ getResultPage permission url gps poffs

frontpage = page getFrontPage
fantasyPage = page getOfklPage
