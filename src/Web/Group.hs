{-# LANGUAGE OverloadedStrings #-}
module Web.Group where

import Control.Monad
import Data.List
import Data.Match (teamsInMatch)
import Data.Maybe
import Data.Text qualified as T
import Data.Types as Types hiding (name)
import Helpers
import Printing (maxNameLength)
import Text.Blaze.Html5 as H hiding (map)
import Text.Blaze.Html5.Attributes as A
import Web.Links
import Web.Results

allGroups gps = do
    headBody "Groups" $ groupViewBuilder gps

groupViewBuilder gps = do
    codeBlock $ do
        let bigger = "font-size: 1.3em;"
        let fields = fieldsForGroups [1..] gps
        H.div ! A.style bigger $
            forM_ (zip3 [1..] fields gps) (uncurry3 buildLineupView)

buildLineupView n fields matches = do
    let groupNum = showT n
    h2 $ toHtml $ "Group " <> groupNum
    fieldText n fields
    br
    buildLineup matches
    let linkStyle = "color: #ff9059"
    H.a ! A.href (textValue groupNum) ! A.style linkStyle $
        toHtml ("Result input page for group " <> showT n)
    H.hr

fieldText n fields = ""
-- fieldText n fields =
--     case fields of
--         [f] ->  toHtml $ "Group " <> showT n <> " plays on field: " <> showT f
--         _ ->  toHtml $
--             "Group "
--             <> showT n
--             <> " plays on fields: "
--             <> T.intercalate ", " (map showT fields)


getRostersInGroup matches =
    nub $ catMaybes $ concatMap ((\(x,y) -> [x,y]) . teamsInMatch) matches

buildLineup matches = do
    let teams = map rosterName $ getRostersInGroup matches
    H.ul $ forM_ teams (H.li . toHtml)


-- FIXME: FIXME: This is a quick hack. Make this somehow sensible.
fieldsRequired matches
    | length matches > 14 = 3
    | length matches > 9 = 2
    | otherwise = 1

fieldsForGroups :: [Int] -> [[Match]] -> [[Int]]
fieldsForGroups numbers (g:gs) = do
    let n = fieldsRequired g
    let (h, t) = splitAt n numbers
    h : fieldsForGroups t gs
fieldsForGroups numbers [] = []

getGroupPage gps n = do
    let pad = maxNameLength gps
    headBody (toHtml ("Group " <> showT n)) $ do
        case filter (\(n',_) -> n == n') (zip [1..] gps) of
            (x:_) ->  codeBlock $ tryBuildPage n x
            _ -> H.div "404 Not Found"



tryBuildPage requestedGroup (n, grp) = do
    let pad = maxNameLength [grp]
    buildResultPage Write (showT n) pad (n, grp)
