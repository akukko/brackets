{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}


module Web.Fantasy
    ( getOfklPage
    , getStats
    , getTotalPlayerPoints
    , getPlayerListing
    , getPointsPerThrow
    , ofkl
    , tfkl
    , Season(..)
    , getPage
    , tfklPlus
    , tfklBonus
    , ofklPlus
    , FantasyStats(..)
    , pointParams
    , getTotalPlayerPointsFromData
    , WebStats
    )
    where

import IO.FetchLeagueResults
import Data.Types
import Helpers
import Format



import Data.List
import qualified Data.Text as T
import Control.Monad
import Data.Bifunctor
import Data.Function
import Text.Show.Pretty
import Data.Ord (Down(..))
import System.Directory

type WebStats a = (a, Int, Double, Double, Double, Double, Double, Double)

dir = "userData/"
regularStatsFile = dir ++ "regularStats"
playoffStatsFile = dir ++ "playoffStats"
latestPlayoffFile = dir ++ "latestPlayoffs"

getStats season = do
    dataDirExists <- doesDirectoryExist dir
    unless dataDirExists $ do
        createDirectory dir

    rExists <- doesFileExist regularStatsFile
    pExists <- doesFileExist playoffStatsFile
    latestExists <- doesFileExist latestPlayoffFile

    unless (latestExists && pExists && rExists) $ do
            (regularStats, playoffStats) <- fetchPlayerStats
            writeFile latestPlayoffFile (show playoffStats)
            writeFile regularStatsFile (show regularStats)
            writeFile playoffStatsFile (show playoffStats)

    getStats' season

getStats' Regular = do
    read <$> readFile regularStatsFile :: IO [(TeamStats, TeamStats)]
getStats' Playoffs = do
    read <$> readFile playoffStatsFile :: IO [(TeamStats, TeamStats)]
getStats' LatestPlayoffs = do
    read <$> readFile latestPlayoffFile :: IO [(TeamStats, TeamStats)]
getStats' All = do
    reg <- read <$> readFile regularStatsFile :: IO [(TeamStats, TeamStats)]
    poffs <- read <$> readFile playoffStatsFile :: IO [(TeamStats, TeamStats)]
    return $ reg ++ poffs

getPointsPerThrow season ptParams = map (join bimap (pointsForTeam ptParams)) <$> getStats season

getTotalPlayerPoints season ptParams = do
    stats <- getStats season
    return $ getTotalPlayerPointsFromData ptParams stats

getTotalPlayerPointsFromData ptParams stats = do
    let perTeamPoints = map (join bimap (pointsForTeam ptParams)) stats
    let perPlayerPoints = map (join bimap ptsPerPlayer) perTeamPoints
    let playerPoints = ptsPerPlayer $ concat $ concatMap (\(x,y) -> [x,y]) perPlayerPoints
    playerPoints

getOfklPage = T.intercalate "\n" <$> getPage Regular ofkl

getPage season params = do
    players <- getTotalPlayerPoints season params
    let longestName = maximum $ map (T.length . fst) players

    let plrListing = getPlayerListing players
    let txt ln plr n left set avgT perc avgP pts =
            padRight ln " " plr <>
            padLeft 6 " " n <>
            padLeft 7 " " left <>
            padLeft 7 " " set <>
            padLeft 6 " " avgT <>
            --padLeft 6 " " perc <>
            padLeft 6 " " avgP <>
            padLeft 8 " " pts

    let plrTxt ln (plr, n, left, set, avgT, perc, avgP, pts) =
                    txt ln plr (T.pack $ show n) (roundFloat 2 left) (roundFloat 2 set)
                            (roundFloat 2 avgT) (roundFloat 2 perc) (roundFloat 2 avgP) (roundFloat 2 pts)

    let titles = txt longestName "player" "throws" "left" "set" "avgT" "perc" "avgP" "fpts"

    return $ (titles : [T.replicate (T.length titles) "-"]) ++ map (plrTxt longestName) plrListing

getPlayerListing :: [(a, FantasyStats)] -> [WebStats a]
getPlayerListing plrs = do
    let totalPts x = sum $ map (\(_, FantasyStats pts _ _ _ _ _) -> pts) x

    let tuples x tot = sortOn (Down . (\(_,_,_,_,_,_,avgP, _) -> avgP)) $
            map (\(plr, FantasyStats pts avgP n left set avgT) -> (plr, n, left, set, avgT, 100 * pts / tot, avgP, pts)) x

    tuples plrs (totalPts plrs)

ptsPerPlayer x = plrPoints $ concat $ groupBy ((==) `on` fst) $ sortOn fst x
    where
        plrPoints [(plr, pts), (plr', pts')]
            | plr == plr' = [(plr, combineStats pts pts')]
            | otherwise = (plr, pts) : [(plr', pts')]
        plrPoints ((plr, pts):(plr', pts'):ps)
            | plr == plr' = plrPoints ((plr, combineStats pts pts'):ps)
            | otherwise = (plr, pts) : plrPoints ((plr', pts'):ps)
        plrPoints _ = []

pointsForTeam ptParams (TeamStats s1 s2) = getFantasyPoints ptParams s1 ++ getFantasyPoints ptParams s2

getFantasyPoints ptParams setStats =
    fpts (points ptParams set errorModifier) startPoints (concat (map fstTwo plrs ++ map lastTwo plrs))
    where
        (SetStats set p1 p2 p3 p4) = setStats
        totalRemoved = setPoints setStats
        startPoints = fromIntegral $ -(start ptParams)
        errorModifier = min startPoints (startPoints + fromIntegral set) / totalRemoved
        plrs = [p1, p2, p3, p4]

fstTwo :: PlayerStats -> [(T.Text, Throw)]
fstTwo (PlayerStats plr t1 t2 _ _ ) = [(plr, t1), (plr, t2)]

lastTwo :: PlayerStats -> [(T.Text, Throw)]
lastTwo (PlayerStats plr _ _ t3 t4) = [(plr, t3), (plr, t4)]

setPoints :: SetStats -> Double
setPoints (SetStats s p1 p2 p3 p4) = sum (map playerPtsRemoved [p1, p2, p3, p4])

playerPtsRemoved :: PlayerStats -> Double
playerPtsRemoved (PlayerStats _ t1 t2 t3 t4) = sum (map effect [t1, t2, t3, t4])

effect :: Throw -> Double
effect (Points x) = fromIntegral x
effect Hauki      = 0
effect Unused     = 0
effect _          = 0

fpts pts left (plr:ps)  = pts plr left : fpts pts (left - effect (snd plr)) ps
fpts _ _ [] = []

toStats left set throw pts = FantasyStats pts pts 1 left (fromIntegral set) (fromIntegral throw)

points :: PointParams -> Int -> Modifier -> (a, Throw) -> Double -> (a, FantasyStats)
points pms set m (plr, Hauki) left = (plr, toStats left set 0 $ hauki pms m left)
points pms set m (plr, Points x) left =
    (plr, toStats left set x $ plus pms (start pms) x m left set)
-- FIXME: How should throw amount be used with unused throws. It might affect the average points.
points pms set m (plr, Unused) left =
    (plr, toStats left set 0 $ unused pms (start pms) m left set)


type Modifier = Double

data Season
    = Regular
    | Playoffs
    | LatestPlayoffs
    | All


data PointParams = PointParams
    { plus      :: Int -> Int -> Modifier -> Double -> Int -> Double
    , hauki     :: Double -> Double -> Double
    , unused    :: Int -> Modifier -> Double -> Int -> Double
    , start     :: Int
    }

data FantasyStats = FantasyStats
    { fantasyPoints :: Double
    , avgPoints     :: Double
    , n             :: Int
    , pointsLeft    :: Double
    , setScore      :: Double
    , throw         :: Double
    } deriving (Show, Eq, Ord)

combineStats :: FantasyStats -> FantasyStats -> FantasyStats
combineStats (FantasyStats pts1 avgP1 n1 left1 set1 avgT1) (FantasyStats pts2 avgP2 n2 left2 set2 avgT2) =
    FantasyStats (pts1 + pts2) (avg ap1 ap2) amt (avg l1 l2) (avg s1 s2) (avg at1 at2)
    where
        amt = n1 + n2
        avg x y = (x + y) / fromIntegral amt
        ap1 = avgP1 * fromIntegral n1
        ap2 = avgP2 * fromIntegral n2
        l1 = left1 * fromIntegral n1
        l2 = left2 * fromIntegral n2
        s1 = set1 * fromIntegral n1
        s2 = set2 * fromIntegral n2
        at1 = avgT1 * fromIntegral n1
        at2 = avgT2 * fromIntegral n2

-- Variation on OFKL points that tries to reward good sweepers more than dirty bombers


n' :: Double
n' = 0.45
a' :: Double
a' = 0.35
b' :: Double
b' = 1.55
c' :: Double
c' = 1.6
h' = 0

tfkl :: PointParams
tfkl = pointParams n' a' b' c' h'

tfklBonus :: Int -> Double -> Int -> Double
tfklBonus = bonus n' a' b' c'

tfklPlus :: Int -> Double -> Double
tfklPlus = plusPoints c'

tfklBonusAndPlus :: Int -> Int -> Double -> Double -> Int -> Double
tfklBonusAndPlus = bonusAndPlus n' a' b' c' h'

tfklBonusAndUnused :: Integer -> Double -> p -> Integer -> Double
tfklBonusAndUnused = bonusAndUnused n' a' b' c'

pointParams :: Double -> Double -> Double -> Double -> Double -> PointParams
pointParams n a b c h = PointParams (bonusAndPlus n a b c h ) (tfklHauki h) (bonusAndUnused n a b c) (-80)

bonusAndPlus n a b c h start pts modifier left set
    | pts == 0 = tfklHauki h mod left
    | otherwise =
    let k = max (modifier * left) 0.1 in
        plusPoints c pts k + bonus n a b c start k set

bonus n a b _ startScore left set =
    let coefficient = a / (1 + nroot b left) in
        (*) coefficient $ divide (set - startScore) (-startScore) ** n

plusPoints c (fromIntegral -> p) left = p / (1 + nroot c left)

tfklHauki :: Floating a => a -> p -> a -> a
tfklHauki h _ left = 0 ---1 * h ** left

bonusAndUnused n a b c s l _ set = bonus n a b c s l set + unusedPoints c

unusedPoints c = plusPoints c 2 4  -- Equivalent to removing one akka with two akka's on the field


-- OFKL points calculation

ofkl :: PointParams
ofkl = PointParams ofklPlusWrapper ofklHauki ofklUnused (-80)

ofklPlusWrapper :: p1 -> Int -> Modifier -> Double -> p2 -> Double
ofklPlusWrapper s p modifier l set = ofklPlus p (modifier * l)

ofklPlus :: Int -> Double -> Double
ofklPlus (fromIntegral -> p) k =
    let x = (p ** 2) + (p / 2) in
        (x / k) + 0.5
    --let k = left in
--   (1 + (2 * p + 1) * ( p / k )) / 2
    --(1 + (fromIntegral (2 * p) + 1) * (f p / k)) / 2

ofklHauki modifier left
    | k > 78 = -7.5
    | otherwise = (-10) * ((k / 80) ** 2)
    where
        k = left * modifier
        --k = left

ofklUnused _ _ _ _ = 0
