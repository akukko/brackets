{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Web.Difference
    ( generateGuesses
    , BetResult(..)
    , generateDifList
    ) where


import Helpers

import Data.List
import qualified Data.Text as T

import Data.HashMap.Strict as HashMap (HashMap, insert, empty, lookup, toList)
import Data.Hashable (Hashable(..))


data BetResult a = BetResult
    { betName           :: a
    , avgError          :: Double
    , correctGuesses    :: (Int, Maybe Int) -- fst is amount of correct guesses, snd is the highest correct guess
    , maxDiff           :: (Int, T.Text)
    } deriving (Eq, Show, Read)

newtype Corrects = Corrects (Int, Maybe Int) deriving (Eq, Show)

instance Eq a => Ord (BetResult a) where
    compare (BetResult _ err correct maxDiff) (BetResult _ error' correct' maxDiff')
        | err > error' = GT
        | err < error' = LT
        | err < error' = LT
        | corrects /= EQ = corrects
        | otherwise = compare maxDiff maxDiff'
        where
            corrects = compare correct correct'

instance Ord Corrects where
    compare (Corrects (a, Just b)) (Corrects (a', Just b'))
        | a > a' = LT
        | a < a' = GT
        | b > b' = LT
        | b < b' = GT
        | otherwise = EQ
    compare (Corrects (a, _)) (Corrects (a', _))
        | a > a' = LT
        | a < a' = GT
        | otherwise = EQ

generateGuesses :: Eq a =>  [(a, [T.Text])] -> [T.Text] -> [BetResult a]
generateGuesses guesses correct = let bets = map snd guesses in
        sort $ map (uncurry4 BetResult) $ zip4 (map fst guesses)
                                (calculateAverageError correct bets)
                                (correctAnswers correct bets)
                                $ maxDiffs correct minBound bets

generateDifList :: [T.Text] -> [T.Text] -> [(Int, T.Text)]
generateDifList ts correct = genDif ts correct 0

genDif (t:ts) correct tIndex = (dif, t) : genDif ts correct (tIndex + 1)
    where
        dif = difference t tIndex correct
genDif [] _ _ = []

generateAverageOrder :: (Eq b, Hashable b) => [[b]] -> [b]
generateAverageOrder xs = map fst $ sortOn snd $ HashMap.toList $ avgOrder HashMap.empty xs

avgOrder ps (x:xs) = avgOrder positions xs
    where
        positions = gatherPositions ps 0 x
avgOrder ps [] = ps

gatherPositions ps p (x:xs) = gatherPositions upd (p + 1) xs
    where
        upd = case HashMap.lookup x ps of
                Just oldP -> HashMap.insert x (oldP + p) ps
                Nothing -> HashMap.insert x p ps
gatherPositions ps p [] = ps

add (a, Just x) (b, _) = (a + b, Just x)
add (a, Nothing) (b, Just x) = (a + b, Just x)
add (a, Nothing) (b, Nothing) = (a + b, Nothing)

calculateCorrectAnswers pos (c:cs) bet =
    add amt $ calculateCorrectAnswers (pos + 1) cs bet
    where
        amt = if absDiff c pos bet == 0 then (1, Just pos) else (0, Nothing)

calculateCorrectAnswers _ [] _ = (0, Nothing)

correctAnswers correct = map (calculateCorrectAnswers 0 correct)


calculateAverageError correct =
    map (flip divide (length correct) . calculateTotalError 0 correct)

calculateTotalError pos (t:ts) bet =
    absDiff t pos bet + calculateTotalError (pos + 1) ts bet
calculateTotalError _ [] _ = 0

maxDiffs :: [T.Text] -> Int -> [[T.Text]] -> [(Int, T.Text)]
maxDiffs correct max' = map (calculateMaxDifference 0 correct (max', ""))

calculateMaxDifference :: Int -> [T.Text] -> (Int, T.Text) -> [T.Text] -> (Int, T.Text)
calculateMaxDifference pos (t:ts) max' bet =
    calculateMaxDifference (pos + 1) ts (max max' (d, t)) bet
    where
        d = absDiff t pos bet
calculateMaxDifference _ [] max' _ = max'

difference :: Eq a => a -> Int -> [a] -> Int
difference team pos ts =
    case elemIndex team ts of
        Just i -> i - pos
        Nothing -> 0

absDiff :: Eq a => a -> Int -> [a] -> Int
absDiff team pos ts = abs $ difference team pos ts
