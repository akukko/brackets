{-# LANGUAGE OverloadedStrings #-}
module Web.Results where

import Web.Links
import Helpers
import Format
import Config (advance)


import Text.Blaze.Html5 qualified as H hiding (map)
import Text.Blaze.Html5 ((!))

import Text.Blaze.Html5.Attributes qualified as A
import Control.Monad
import qualified Data.Text as T
-- import Network.URI

import Data.Types as Types hiding (name)
import Printing ( maxNameLength
                , showPrettyTables
                , showPrettyStandings
                )
import qualified Data.List.NonEmpty as NE
import Control.Monad.Extra (whenJust)



resultsUrl = "results"


getResultPage permission url gps poffs = do
    let pad = maxNameLength gps

    let hb = case permission of
            Rewrite -> adminHeadBody
            _ -> headBody

    hb "Results" $ do
        -- Playoffs
        case poffs of
            ((x:xs):ys) ->
                playoffGames (playoffPermission permission) "Playoffs" url poffs
            _ -> return ()
        -- Overall standings
        let standings = showPrettyStandings advance =<< NE.nonEmpty gps
        whenJust standings $ collapsible "Overall standings"
        -- Results
        codeBlock $ forM_ (zip [1..] gps) (buildResultPage permission url pad)

playoffGames :: Permission -> H.Html -> T.Text ->  [[Match]] -> H.Html
playoffGames permission title url pMatches = do
    let pad = maxNameLength pMatches
    codeBlock $ do
        H.h2 title
        forM_ pMatches $ \rnd -> do
            forM_ rnd (\m -> toResultInput m permission pad addNewlines url)
            H.hr

collapsible title content =
    codeBlock $ H.details $ do
        let bigBold = "font-size: 1.6em; font-weight: bold"
        H.summary ! A.style bigBold $ title
        H.div $ H.toHtml (T.intercalate "\n" content)
        replicateM_ 4 H.br


buildResultPage write url pad (n, group) = do
    resetUrl url
    let table = showPrettyTables $ group NE.:| []
    H.h2 ("Group " <> H.toHtml (showT n))
    whenJust table $ H.div . H.toHtml . T.intercalate "\n"
    forM_ group (\m -> toResultInput m write pad n url)
    H.hr

fromMatch mup = do
    let textId = showT $ mid $ matchID mup
    let h = rosterName . home $ mup
    let a = rosterName . away $ mup

    (textId, h, a)

toResultInput (Finished mup (Tieable (Scores hs as))) =
    uncurry3 matchResultInput (fromMatch mup) (showT hs) (showT as)
toResultInput (Finished mup (NonTieable (Scores hs as) _)) =
    uncurry3 matchResultInput (fromMatch mup) (showT hs) (showT as)
toResultInput (Finished mup _) = -- FIXME: Add series
    uncurry3 matchResultInput (fromMatch mup) "" ""
toResultInput (Scheduled _ mup) = uncurry3 matchResultInput (fromMatch mup) "" ""
toResultInput (Unscheduled _) = tbaResultInput tba tba
toResultInput (HomeOnly _ Roster {rosterName = h}) = tbaResultInput h tba
toResultInput (AwayOnly _ Roster {rosterName = a}) = tbaResultInput tba a

tba = "TBA"

tbaResultInput home away _ pad _ _ = do
    H.toHtml $ padLeft pad " " home <> " "
    scoreInputs Read "" ""
    H.toHtml $ " " <> padRight pad " " away
    H.br


matchResultInput mid home away hs as write pad n url = do
    let compactForm = H.form ! A.style "margin: 0px;"
    compactForm ! A.action (H.textValue url) $ do
        H.label ! A.for "home" $ H.toHtml $ padLeft pad " " home <> " "
        H.input ! A.hidden "" ! A.name "matchId" ! A.value (H.textValue mid)
        scoreInputs write hs as
        H.label ! A.for "away" $ H.toHtml $ " " <> padRight pad " " away


data Permission = Rewrite | Write | Read

playoffPermission Read = Write
playoffPermission x = x

scoreInputs Write "" "" = scoreInputs Rewrite "" ""
scoreInputs Write hs as = scoreInputs Read hs as
scoreInputs Rewrite hs as = do
    scoreInput "home" hs
    H.input ! A.type_ "submit" ! A.style "padding: 1px;" ! A.value "Add"
    scoreInput "away" as
scoreInputs Read hs as = do
    let pad = 6
    H.toHtml $ T.unwords [ padLeft pad " " hs
                       , "  - "
                       , padRight pad " " (padLeft 3 " " as)
                       ]

scoreInput n score =
    H.input ! A.type_ "number" ! A.min "-160" ! A.max "38"
          ! A.name n ! A.required "score"
          ! A.style "width: 50px" ! A.value (H.textValue score)
