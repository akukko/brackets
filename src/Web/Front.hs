
{-# LANGUAGE OverloadedStrings #-}

module Web.Front (getFrontPage) where


import IO.FetchLeagueResults
import Printing
import Scoreboard

import Data.Maybe

import Data.Text as T

getFrontPage = do
    (maybeRegular, maybePlayoffs) <- fetchLeagueResults

    let regSeasonGames = fromMaybe [] maybeRegular
    let playoffs = fromMaybe [] maybePlayoffs
    
    let scores = buildScoreboards [regSeasonGames]

    let orderedPlayoffs = reorder scores playoffs


    let printable = Prelude.map (T.intercalate "\n") $ prettyScoreboards scores
    let poffs = prettyDoubleElim playoffs
    return $ T.intercalate "\n" printable <> "\n\n" <> poffs


reorder scores playoffs = []
