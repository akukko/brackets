{-# LANGUAGE OverloadedStrings #-}
module Web.Admin where

import Config
import Control.Monad
import Data.ByteString.Lazy (ByteString)
import Data.List
import Data.Match (removeScore)
import Data.Text qualified as T
import Data.Types as Types hiding (name)
import Data.Vault
import Groups
import Helpers
import IO.ReadTeams
import Scheduling (getSchedule)
import Text.Blaze.Html5 as H hiding (map)
import Text.Blaze.Html5.Attributes as A
import Text.Show.Pretty (pPrint)
import Web.Group
import Web.Links


adminPanel gps = do
    adminHeadBody "Admin panel" $ do
        let teams = map getRostersInGroup gps
        let groups = map (map rosterName) teams

        H.form ! action "regenerate" $ do
            let js = "return confirm('Destroy existing results and \
                     \regenerate groups?')"
            input ! type_ "submit" ! onclick js ! value "Regenerate"
            forM_ (zip [1..] groups) $ \(n, grp) -> do
                h3 (toHtml $ "Group " <> showT n)
                forM_ grp $ \team -> do
                    let t = textValue team
                    input ! type_ "checkbox" ! name "remove" ! value t
                    H.label $ toHtml $ " " <> team
                    br

regenerateGroups :: [T.Text] -> [[Match]] -> IO ByteString
regenerateGroups teamsToRemove oldGroups = do
    let oldRostersPerGroup = map getRostersInGroup oldGroups
    let teams = concat $ transpose oldRostersPerGroup
    let without = filter (\n -> rosterName n `notElem` teamsToRemove) teams

    -- FIXME: FIXME: This is a quick hack to get the seeded teams somehow
    let teamFile = "userData/ruskakyykka-2022-teams"
    ts <- readTeams <$> readFile teamFile
    -- / Hack end
    let indexTeams =  [1..(length ts)]
    let seededRosters = zipWith (\t tid -> Roster (TeamID tid) t []) ts indexTeams
    let newSeeded =
            filter (\n -> rosterName n `elem` map rosterName without) seededRosters

    pPrint seededRosters

    schedule <-
        if null teamsToRemove
        -- Empty regen call, clear results
        then return $ map (map removeScore) oldGroups
        else
            if without /= teams
                -- had some teams to remove
                then do
                    newGroups <- shuffleGroups newSeeded
                    return $ getSchedule newGroups balancing (MatchID competitionId 0)
                -- Nothing to remove, accidental regen call
                else return oldGroups

    let c' = Competition competitionId schedule []
    saveCompetition c'

    return $ adminHeadBody "Regenerate" $ do
        resetUrl "panel"
        h1 ! A.style "color: yellow" $ "Regeneration complete"
        groupViewBuilder schedule

shuffleGroups teams = do
    shuffledTeams <- getShuffled teams seedingBasketSize seedingBasketsAmount
    return $ getGroups shuffledTeams groupFormat

{-
    --let newTeams = filter (\n -> rosterName n `notElem` teamsToRemove) teams
    --let newGroups = getGroups newTeams groupFormat
    liftIO $ print teamsToRemove
    liftIO $ print $ map rosterName $ concat without
    liftIO $ putStrLn "\n"

    let newGroups = rebalanceGroups without
    liftIO $ print newGroups
    liftIO $ putStrLn "\n"
    return ""


-- FIXME: FIXME: This only works on this specific scenario (if even then).
--              Make a generic solution to the problem.
rebalanceGroups gps
    | all (\g -> length g `elem` [4, 5]) gps = gps
    | noTwoGroups && balancingPossible = gps
          --zipWith (\l tg -> tg ++ [l]) (map last fiveGroups) threeGroups
    | otherwise = gps
    where
        noTwoGroups = all (\g -> length g `elem` [3, 4, 5]) gps
        balancingPossible = length threeGroups <= length fiveGroups
        threeGroups = filter (\g -> length g == 3) gps
        fiveGroups = filter (\g -> length g == 5) gps

-}
