{-# LANGUAGE OverloadedStrings #-}

module Scoreboard
    ( TeamScore (..)
    , buildScoreboards
    , buildEmptyScoreboards
    , getFinalStandings
    , getRanked
    ) where

import Data.Score
import Data.Types as Types
import Data.TeamScore
import Helpers

import Data.List
import qualified Data.Text as Text ( Text, length, intercalate, replicate, concat, pack )
import Data.Maybe
import qualified Data.HashMap.Strict as HashMap (HashMap, insert, empty, lookup, toList)
import TextShow ( showt, toText )
import qualified Data.Set as Set
import qualified Config

{-
-- FIXME: Figure out how to do this in a sane way.
buildScoreboardsFromSeries builder ss = builder $ map (mapMaybe filterTieable) ss
    where filterTieable m = case m of
            Tieable m -> Just m
            _ -> Nothing
-}
buildScoreboards :: [[Match]] -> [[TeamScore]]
buildScoreboards ms = map (sortScores . buildScore HashMap.empty) results
    where
      filterResults match = case match of
          -- FIXME: Handle non-tieable and series
          Finished m (Tieable s) -> Just (m, s)
          _ -> Nothing
      results = map (mapMaybe filterResults) ms

buildEmptyScoreboards :: [[Match]] -> [[TeamScore]]
buildEmptyScoreboards = map (buildEmpty Set.empty) . matchups
    where
        filterResults match = case match of
            Scheduled _ matchup -> Just matchup
            _ -> Nothing
        matchups = map (mapMaybe filterResults)

        buildEmpty teams (m:ms) = buildEmpty (Set.insert a $ Set.insert h teams) ms
            where
                h = Types.home m
                a = Types.away m
        buildEmpty teams [] = map (`TeamScore` []) $ Set.toList teams


buildScore :: HashMap.HashMap Roster [(Matchup, Score)]
           -> [(Matchup, Score)] -> [TeamScore]
buildScore teams ((m, s):rs) = buildScore teams'' rs
    where
        h = Types.home m
        a = Types.away m
        teams' = case HashMap.lookup h teams of
                Just results -> HashMap.insert h (results++[(m,s)]) teams
                Nothing -> HashMap.insert h [(m,s)] teams
        teams'' = case HashMap.lookup a teams' of
            Just results -> HashMap.insert a (results++[(m,s)]) teams'
            Nothing -> HashMap.insert a [(m,s)] teams'
buildScore teams [] = map (uncurry TeamScore) $ HashMap.toList teams



getRanked =
    take Config.advance . map thd . getFinalStandings


getFinalStandings :: [[TeamScore]] -> [(Int, Int, TeamScore)]
getFinalStandings ts = concatMap (sortOn thd) withGroupAndRank
    where
        withGroupAndRank = transpose $
            map (\(gi,gs) -> map (\(ti,t) -> (gi, ti, t)) gs) teamsWithIndex
        teamsWithIndex =
            zip [1..] (map (zip [1..]) ts) :: [(Int, [(Int, TeamScore)])]
