{-# LANGUAGE OverloadedStrings #-}


module Interactive
    ( printCompetition
    , printCompetitions)
    where


import Helpers
import Groups
import Data.Result
import Brackets
import IO.ReadResults
import IO.Write
import IO.Locations

import Data.Vault
import Config
import Data.Competition
import IO.ReadTeams
import Scheduling
import Scoreboard
import Printing
import Format
import Data.Types as Types
import Data.Match as Match

import Control.Monad
import Control.Monad.State
import Data.Ord
import Data.List as List
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import TextShow ( showt, printT )
import Data.HashMap.Strict as HashMap (HashMap, insert, empty, lookup, elems)
import Data.Maybe

import Debug.Trace
import Text.Show.Pretty

-- Public functions

printCompetition :: Competition -> IO ()
printCompetition c = s' (Just c) Nothing


printCompetitions c c' = s' (Just c) (Just c')


-- Private functions




defaultTeams = ["Disko_Lisko", "anaalivandaalit", "DDRNV", "Esso-XL",
                "SuHa", "AiM", "Tekla", "BlaBla"]

tc :: IO Competition
tc = alwaysGetCompetition defaultTeams Config.competitionId

clear = saveVault emptyVault

add hTeam aTeam hScore aScore = do
    c <- tc
    let c' = tryUpdateMatch (T.pack hTeam) (T.pack aTeam) (Scores hScore aScore) c
    saveCompetition c'
    s' (Just c) (Just c')


i :: Int -> Int -> IO ()
i h a = do
    c <- tc
    input c h a

input :: Competition -> Int -> Int -> IO ()
input c h a = do
    let c' = tryUpdateNextMatch (Scores h a) c
    saveCompetition c'

    s' (Just c) (Just c')

change h a = do
    c <- tc
    let prev = getLastMatch c
    let c' = maybe c (\m -> addScore (Scores h a) m c) prev
    saveCompetition c'
    s' (Just c) (Just c')

pop = do
    c <- tc
    let c' = tryDeletePreviousScore c
    saveCompetition c'
    s' (Just c) (Just c')

newComp :: [Char] -> Int -> IO ()
newComp name season = do
    let cid = CompetitionID (T.pack name) season
    let c = Competition cid [] []

    trace (ppShow c) $ saveCompetition c

addTeams :: [Char] -> Int -> FilePath -> IO ()
addTeams name season teamFile = do
    ts <- readTeams <$> readFile teamFile
    let cid = CompetitionID (T.pack name) season
    let c = generateTournament cid TwelveFields ts

    trace (ppShow c) $ saveCompetition c

matches :: IO ()
matches = do
    c <- tryGetCompetition competitionId
    let ms = maybeMatches c
    let txts = (\ms' -> map (map (prettyMatch (maxNameLength ms'))) ms') <$> ms
    let combined = ("\nGroups\n" :) . List.intercalate ["\nPlayoffs\n"] <$> txts
    forM_ combined (mapM TIO.putStrLn)

maybeMatches c = do
    g <- concat . groups <$> c
    p <- concat . playoffs <$> c
    return $ g : [p]


--s = (`s'` Nothing) =<< tryGetCompetition competitionId


s' :: Maybe Competition -> Maybe Competition -> IO ()
s' c c' = do
    t c c'

    let f = showPrettyPlayoffs =<< safePlayoffs =<< c
    let s = showPrettyPlayoffs =<< safePlayoffs =<< c'

    let pad = 6
    -- 25 happens to be the magic number to horizontally
    -- match group stage scoreboards with the playoff schedules.
    let magic = 25
    case (f, s) of
        (Just a, Just b) -> mapM_ TIO.putStrLn $ sideBySide (magic+pad) a b
        (Just a, Nothing) -> mapM_ TIO.putStrLn a
        (Nothing, Just b) ->
            let padding = map (\tm -> T.replicate (T.length tm) ".") b in
                mapM_ TIO.putStrLn $ sideBySide (magic+pad) padding b
        _ -> return ()

poffs = do
    c <- firstComp
    let maybetxt = showPrettyPlayoffs =<< safePlayoffs =<< c
    forM_ maybetxt (mapM_ TIO.putStr)
    putStrLn ""

standings = do
    v <- getVault
    let gps = maybe [] groups (HashMap.lookup competitionId (competitions v))
    forM_ (showPrettyStandings advance =<< NE.nonEmpty gps) (mapM_ TIO.putStrLn)

table :: IO ()
table = flip t Nothing =<< tryGetCompetition competitionId

t c c' = do
    let f = showPrettyScoreboards =<< safeGroups =<< c
    let s = showPrettyScoreboards =<< safeGroups =<< c'

    case (f, s) of
        (Just a, Just b) -> mapM_ TIO.putStrLn $ sideBySide 6 a b
        (Just a, Nothing) -> mapM_ TIO.putStrLn a
        _ -> return ()


safeGroups = NE.nonEmpty . groups
safePlayoffs = NE.nonEmpty . playoffs


--refresh comp = do




groupStage :: String -> Int -> IO ()
groupStage name season = do
    groupStageWithCid $ CompetitionID (T.pack name) season

groupStageWithCid :: CompetitionID -> IO ()
groupStageWithCid cid = do
    v <- getVault
    let gps = maybe [] groups (HashMap.lookup cid (competitions v))
    trace (ppShow gps) return ()


comp = do
    v <- getVault
    let cps = HashMap.elems (competitions v)
    mapM_ (pPrint . competitionID) cps

comps = HashMap.elems . competitions <$> getVault

firstComp :: IO (Maybe Competition)
firstComp = (listToMaybe <$> HashMap.elems) . competitions <$> getVault
