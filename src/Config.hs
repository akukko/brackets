{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Config ( maxTeams
              , advance
              , balancing
              , Balancing(..)
              , competitionName
              , competitionSeason
              , GroupStageFormat(..)
              , PlayoffFormat(..)
              , groupFormat
              , playoffMatchFormat
              , seedingBasketSize
              , seedingBasketsAmount
              , competitionId
              ) where

import Data.Text
import Data.Types
import Data.Result

data Balancing
    = Sparse
    -- | Two games played concurrently
    | ConcurrentTwo
    -- | Four game played concurrently
    | ConcurrentFour
    -- | The league format of scheduling allows two simulatenous games and sets limits to minimum and maximum amount of games per day.
    | League
            Int  -- ^ The minimum amount of games per team per day
            Int  -- ^ The maximum amount of games per team per day
    | NoBalancing deriving (Show)


-- FIXME: There could be an flag where the most suitable option is selected based
--       on team amount.
--       Otherwise the rules of the tournament format should be strictly
--       specified so that the software is guaranteed to always work.

-- FIXME: The field amount should probably be the most important argument here.
--       That basically determines what kind of
--       formats are possible in the first place.
--       The maximum amount of rounds in group stage or games per
--       team might also be good arguments.

data GroupStageFormat
    = TwelveFields
    | TenFields
    | SingleRoundRobin
    | ThreeGroups
    | ReadyGroupsOf Int
    deriving (Show)

data PlayoffFormat
    = SingleElimination
    | DoubleElimination
    deriving (Show)


-- FIXME: Refactor the configs

-- Build such parameters that they work always.
-- For example use algebraic data types if that helps.

-- ConcurrentFour should probably inherently determine
-- the number of teams advancing from the group

-- One possibility is to remove the maxTeams and advance parameters entirely, and
-- only use the Balancing type to cover the most commonly used formats.

seedingBasketSize :: Int
seedingBasketSize = 0

seedingBasketsAmount :: Int
seedingBasketsAmount = 2

maxTeams :: Int
maxTeams = 9 :: Int

advance :: Int
advance = 4 :: Int

balancing :: Balancing
balancing = ConcurrentTwo

groupFormat :: GroupStageFormat
--groupFormat = ReadyGroupsOf
groupFormat = TwelveFields


playoffFormat :: PlayoffFormat
playoffFormat = DoubleElimination

playoffMatchFormat = NonTie

competitionName :: Text
competitionName = "Pariliiga"

competitionSeason :: Int
competitionSeason = 2023

competitionId :: CompetitionID
competitionId = CompetitionID competitionName competitionSeason

