{-# LANGUAGE OverloadedStrings #-}

module Scheduling
    ( getSchedule
    , getScheduleForGroup
    , getMatchups
    , recurse
    , januaryDays
    , fitsAll
    , fittingDays
    ) where


import Data.Types
import Data.Match
import Balance
import Format
import Config
import Printing
import Helpers

import Data.List
import Data.Text as Text ( Text )
import Data.Time
import Data.Time.Calendar.WeekDate
import Control.Monad.State

-- Takes a list of groups.
-- Returns a list of groups' matchups.
getSchedule :: [[Roster]] -> Balancing -> MatchID -> [[Match]]
getSchedule g bt mid = map (map (Scheduled Tie)) (getMatchups g bt mid)


getMatchups g bt mid = reverse $ balanceMatches bt $ evalState (recurse g []) mid

recurse ::  [[Roster]] -> [[Matchup]] -> State MatchID [[Matchup]]
recurse (t:ts) a = do
    gs <- getScheduleRec t []
    recurse ts (gs : a)
recurse [] a = return a

getScheduleForGroup :: [Roster] -> MatchID -> [Matchup]
getScheduleForGroup t = evalState (getScheduleRec t [])

getScheduleRec :: [Roster] -> [Matchup] -> State MatchID [Matchup]
getScheduleRec (x:xs) a = do
    mups <- createMatchups [] x xs
    getScheduleRec xs (mups ++ a)

getScheduleRec [] x = return x

createMatchups :: [Matchup] -> Roster -> [Roster] -> State MatchID [Matchup]
createMatchups a c (t:ts) = do
    mid <- mods incr
    createMatchups (Matchup mid c t:a) c ts

createMatchups a c [] = return a

fitsAll :: [[Day]] -> [Day]
fitsAll [d] = d
fitsAll (d:ds) = intersect d $ fitsAll ds
fitsAll [] = []

fittingDays :: [Day] -> [Day] -> [Day]
fittingDays possibleDays unfitDays = possibleDays \\ unfitDays


januaryDays :: [Day]
januaryDays = filter (weekday . dayOfWeek) $ map (fromGregorian 2022 1) [8..31]

weekday :: DayOfWeek -> Bool
weekday Saturday = False
weekday Sunday = False
weekday _ = True
