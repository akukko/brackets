{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module Data.Types
            ( Match(..)
            , Matchup(..)
            , MatchID(..)
            , Result(..)
            , Player(..)
            , TeamID(..)
            , Team(..)
            , PlayerID(..)
            , Competition(..)
            , CompetitionID(..)
            , Vault(..)
            , Score(..)
            , Teams
            , Players
            , Competitions
            , Roster(..)
            --, Vault(..)
            , Bracket(..)
            , Matchday
            , Date(..)
            , Time(..)
            , Residence(..)
            , PlayerStats(..)
            , SetStats(..)
            , TeamStats(..)
            , Throw(..)
            , Res(..)
            ) where

import Data.HashMap.Strict as HashMap (HashMap)
import Data.Hashable (Hashable)
import Data.List.NonEmpty
import Data.Text (Text)
import GHC.Generics (Generic)
import TextShow.TH

data CompetitionID = CompetitionID
    { name   :: Text
    , season :: Int
    } deriving (Show, Generic, Eq, Hashable, Ord, Read)
$(deriveTextShow ''CompetitionID)

data MatchID = MatchID
    { relatedCompetition :: CompetitionID
    , mid                :: Int
    } deriving (Show, Generic, Eq, Hashable, Ord, Read)
$(deriveTextShow ''MatchID)

newtype PlayerID = PlayerID { pid :: Int } deriving (Show, Generic, Eq, Read, Hashable, Ord)
$(deriveTextShow ''PlayerID)

newtype TeamID = TeamID { tid :: Int } deriving (Show, Generic, Eq, Hashable, Read, Ord)
$(deriveTextShow ''TeamID)

type Teams = HashMap TeamID Team
type Competitions = HashMap CompetitionID Competition
type Players = HashMap PlayerID Player

data Vault = Vault
    { competitions :: Competitions
    , teams        :: Teams  -- FIXME: Consider if storing these separately makes sense.
                             --       The same data is already in the competitions.
    , players      :: Players
    } deriving (Show)

data Competition = Competition
    { competitionID :: CompetitionID
    , groups        :: [[Match]]
    , playoffs      :: [[Match]]
    } deriving (Show, Generic)

data Player = Player
    { playerID   :: PlayerID
    , playerName :: Text
    --, rounds :: Int  -- the unit is phi
    } deriving (Show, Eq, Read, Hashable, Ord, Generic)

-- Higher level structure that holds data of a single team in all competitions
data Team = Team
    { teamID    :: TeamID
    , teamName  :: Text     -- The most used and recognizable name for the team
    , rosters   :: HashMap CompetitionID Roster
    } deriving (Show)


-- Team in one single competition
data Roster = Roster
    { relatedTeam   :: TeamID
    , rosterName    :: Text  -- The competition specific team name that might
                             -- differ from the most used one.
    , members       :: [Player]
    } deriving (Show, Eq, Read, Hashable, Ord, Generic)

-- Players representing a team in a single set
data Side
    = Quatro (Player, Player, Player, Player)
    | Trio (Player, Player, Player)
    | Duo (Player, Player)
    | Solo Player

-- Players representing a team in a single game
data Lineup = Lineup Roster (Side, Side)


data Bracket
    = Seeded Match
    | WinnersOf Bracket Bracket
    --  | LoserAndWinnerOf Series Series
    --  | LoserAndSeededOf Series Roster

data Result
    = Tieable Score
    -- FIXME: take into account multiple tiebreaker games.
    --       Maybe a recursive field in Result or a new recursive type?
    | NonTieable Score (Maybe Score)
    | Series (NonEmpty Score) deriving (Show, Eq, Generic)

data Res = Tie | NonTie | Serie deriving (Show, Eq, Generic)

data Match
    = Scheduled Res Matchup
    | Finished Matchup Result
    | HomeOnly MatchID Roster
    | AwayOnly MatchID Roster
    | Unscheduled MatchID deriving (Show, Eq, Generic)

data Matchup = Matchup
    { matchID   :: MatchID
    , home      :: Roster
    , away      :: Roster
    } deriving (Show, Eq, Generic)

data Score --home away
    = Scores Int Int
    | Default Residence -- Home/Away won because opponent forfeited
    deriving (Show, Eq, Generic)


data Residence = Home | Away deriving (Show, Eq, Generic)

-- | List of a single group's matchups on a given day
type Matchday = (Date, [Matchup])

type DateTime = (Date, Time)

data Date = Date
    { year  :: Int
    , month :: Int
    , day   :: Int
    }

data Time = Time
    { hour      :: Int
    , minute    :: Int
    }

data PlayerStats = PlayerStats Text Throw Throw Throw Throw deriving (Show, Read)

data SetStats =
    SetStats Int
        PlayerStats
        PlayerStats
        PlayerStats
        PlayerStats deriving (Show, Read)

data TeamStats = TeamStats SetStats SetStats deriving (Show, Read)

data Throw = Throw
    | Points Int
    | Hauki
    | Unused
    deriving (Show, Read)

