{-# LANGUAGE DeriveGeneric #-}

module Data.Vault
    ( emptyVault
    , BinaryVault(..)

    , toBinaryVault
    , fromBinaryVault
    , getVault
    , getVaultFrom
    , saveVault
    , saveVaultTo
    , addTeamsToVault
    , saveCompetition
    , addCompetitionToVault
    , addMatchToVault
    , saveMatchResult
    , alwaysGetCompetition
    , tryGetCompetition
    ) where


import Data.Types
import Data.Competition
import IO.Locations
import Data.Match as Match
import Config

import Data.Bifunctor ( Bifunctor(second) )
import Data.HashMap.Strict as HashMap
    ( HashMap, empty, toList, fromList, insert, lookup )
import Data.Text ( Text, unpack, pack)
import Data.Binary ( Binary, encode, decode )
import GHC.Generics ( Generic )
import qualified Data.ByteString.Lazy as BS
import System.Directory
import Control.Exception
import Control.DeepSeq

-- Public vault modifiers

-- | Save competition to disk
saveCompetition :: Competition -> IO ()
saveCompetition c = do
    v <- getVault
    let v' = addCompetitionToVault c v
    saveVault v'

-- | Save match result to disk
saveMatchResult mid hScore aScore = do
    v <- getVault
    let v' = addScoreToVault v mid $ Scores hScore aScore
    saveVault v'

-- Getters

alwaysGetCompetition :: [Text] -> CompetitionID -> IO Competition
alwaysGetCompetition ts cid = do
    v <- getVault
    case HashMap.lookup cid $ competitions v of
        Just comp -> return comp
        Nothing -> do
            let new = generateTournament cid groupFormat ts
            saveVault $ addCompetitionToVault new v
            return new

tryGetCompetition :: CompetitionID -> IO (Maybe Competition)
tryGetCompetition cid = HashMap.lookup cid . competitions <$> getVault

-- --

emptyVault :: Vault
emptyVault = Vault
    { competitions = HashMap.empty
    , teams = HashMap.empty
    , players = HashMap.empty
    }

addTeamToVault :: (TeamID, Text) -> Vault -> Vault
addTeamToVault (tid, teamName) vault =
    let team = Team tid teamName HashMap.empty in
    vault { teams = HashMap.insert tid team (teams vault) }


addTeamsToVault :: Vault -> [Text] ->  (Vault, [TeamID])
addTeamsToVault vault teamNames  = do
    let startID = length (teams vault)
    let ids = map TeamID [startID + 1 .. startID + length teamNames]
    let zipped = zip ids teamNames
    (foldl (flip addTeamToVault) vault zipped, ids)

--  | This overwrites any existing competition with the same ID
addCompetitionToVault :: Competition -> Vault -> Vault
addCompetitionToVault comp vault =
    vault { competitions =
            HashMap.insert (competitionID comp) comp (competitions vault) }

addScoreToVault :: Vault -> MatchID -> Score -> Vault
addScoreToVault vault mid score = do
    let cid = relatedCompetition mid
    case HashMap.lookup cid $ competitions vault of
        Just comp -> do
            case tryGetMatch mid comp of
                Just match ->
                    addCompetitionToVault (updateMatch match comp) vault
                Nothing -> vault
        Nothing -> vault

addMatchToVault :: Vault -> Match -> Vault
addMatchToVault vault match = do
    let cid = relatedCompetition $ Match.matchID match
    case HashMap.lookup cid $ competitions vault of
            Just comp -> addCompetitionToVault (updateMatch match comp) vault
            Nothing -> vault


-- File io

getVault :: IO Vault
getVault = getVaultFrom vaultFile

saveVault :: Vault -> IO ()
saveVault = saveVaultTo vaultFile

getVaultFrom :: FilePath -> IO Vault
getVaultFrom file = do
    exists <- doesFileExist file
    if exists then do
        bytes <- BS.readFile file
        evaluate (force bytes)
        return $ fromBinaryVault (decode bytes :: BinaryVault)
    else do
        saveVaultTo file emptyVault
        return emptyVault

saveVaultTo :: FilePath -> Vault -> IO ()
saveVaultTo file vault = do
    let bytes = encode (toBinaryVault vault)
    BS.writeFile file bytes

-- Binary encoding helpers

data BinaryVault = BinaryVault
    { b_competitions :: [(CompetitionID, Competition)]
    , b_teams :: [(TeamID, BinaryTeam)]
    , b_players :: [(PlayerID, Player)]
    } deriving (Show, Generic)

data BinaryTeam = BinaryTeam
    { b_teamID :: TeamID
    , b_teamName :: Text
    , b_rosters :: [(CompetitionID, Roster)]
    } deriving (Show, Generic)



instance Binary MatchID
instance Binary Matchup
instance Binary Result
instance Binary Res
instance Binary Match
instance Binary Score
instance Binary Residence
instance Binary CompetitionID
instance Binary Competition
instance Binary TeamID
instance Binary Roster
instance Binary BinaryTeam
instance Binary PlayerID
instance Binary Player

instance Binary BinaryVault

toBinaryVault :: Vault -> BinaryVault
toBinaryVault v = BinaryVault
    { b_competitions = toList (competitions v)
    , b_teams = map (Data.Bifunctor.second toBinaryTeam) (toList (teams v))
    , b_players = toList (players v)
    }

fromBinaryVault :: BinaryVault -> Vault
fromBinaryVault bv = Vault
    { competitions = fromList (b_competitions bv)
    , teams = fromList $ map (Data.Bifunctor.second fromBinaryTeam) (b_teams bv)
    , players = fromList (b_players bv)
    }

toBinaryTeam :: Team -> BinaryTeam
toBinaryTeam t = BinaryTeam
    { b_teamID = teamID t
    , b_teamName = teamName t
    , b_rosters = toList (rosters t)
    }

fromBinaryTeam :: BinaryTeam -> Team
fromBinaryTeam bt = Team
    { teamID = b_teamID bt
    , teamName = b_teamName bt
    , rosters = fromList (b_rosters bt)
    }

