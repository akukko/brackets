{-# LANGUAGE OverloadedStrings #-}


module Data.Match
    ( winner
    , sameMatches
    , incr
    , Data.Match.matchID
    , isFinished
    , isHomeOnly
    , sameMatch
    , canOverwrite
    , updateScore
    , removeScore
    , sameMatchList
    , longerName
    , gameAmount
    , teamNamesMatch
    , teamsInMatch
    , nextScheduled
    , firstFinished
    , lastMid
    , sameMatchup
    ) where

import Data.Types as Types
import Data.Result as Result
import Helpers ((|>))
import Data.List.NonEmpty as NE (NonEmpty(..), sort, head, map, reverse)

import qualified Data.Text as T
import Debug.Trace


longerName :: Match -> T.Text
longerName (Finished Matchup {home = Roster {rosterName = h},
                              away =  Roster {rosterName = a}} _)
    | T.length h > T.length a = h
    | otherwise = a
longerName (Scheduled _ Matchup {home = Roster {rosterName = h},
                               away =  Roster {rosterName = a}})
    | T.length h > T.length a = h
    | otherwise = a
longerName (HomeOnly _ Roster {rosterName = n}) = n
longerName (AwayOnly _ Roster {rosterName = n}) = n
longerName _  = ""

matchID :: Match -> MatchID
matchID (Finished m _)  = Types.matchID m
matchID (Scheduled _ m)   = Types.matchID m
matchID (HomeOnly m _)  = m
matchID (AwayOnly m _)  = m
matchID (Unscheduled m) = m

teamsInMatch :: Match -> (Maybe Roster, Maybe Roster)
teamsInMatch (Finished m _) = (Just $ home m, Just $ away m)
teamsInMatch (Scheduled _ m) = (Just $ home m, Just $ away m)
teamsInMatch (HomeOnly _ h) = (Just h, Nothing)
teamsInMatch (AwayOnly _ a) = (Nothing, Just a)
teamsInMatch (Unscheduled _) = (Nothing, Nothing)

sameMatchup :: Match -> Match -> Bool
sameMatchup a b = teamsInMatch a == teamsInMatch b

winner :: Match -> Maybe Roster
winner r = case r of
    Finished m res ->
        case Result.findWinner res of
            Just Home -> Just $ Types.home m
            Just Away -> Just $ Types.away m
            Nothing   -> Nothing

    Scheduled _ _   -> Nothing
    HomeOnly _ _  -> Nothing
    AwayOnly _ _  -> Nothing
    Unscheduled _ -> Nothing

incr :: MatchID -> MatchID
incr m = m { mid = mid m + 1 }


teamNamesMatch :: T.Text -> T.Text -> Match -> Bool
teamNamesMatch t1 t2 m = case teamsInMatch m of
    (Just home, Just away) -> rosterName home == t1 && rosterName away == t2
    _ -> False

sameMatches :: [Match] -> [Match] -> Bool
sameMatches (x:xs) (y:ys)
    | xm == ym = sameMatches xs ys
    | otherwise = False
    where
        xm = Data.Match.matchID x
        ym = Data.Match.matchID y
sameMatches [] [] = True
sameMatches [] _ = False
sameMatches _ [] = False

isFinished :: Match -> Bool
isFinished (Finished _ _) = True  -- FIXME: Think through (series?)
isFinished _              = False

isUnscheduled :: Match -> Bool
isUnscheduled (Unscheduled _) = True
isUnscheduled _               = False

isHomeOnly :: Match -> Bool
isHomeOnly (HomeOnly _ _) = True
isHomeOnly _               = False

gameAmount :: Match -> Int
gameAmount (Finished _ (Series (s :| ss))) = length (s:ss)
gameAmount (Finished _ (Tieable _)) = 1
gameAmount (Finished _ (NonTieable _ _)) = 1
gameAmount _ = 0

updateScore :: Score -> Match -> Match
updateScore s (Finished m (Tieable _))      = Finished m (Tieable s)
updateScore s (Finished m (NonTieable _ _)) = Finished m (NonTieable s Nothing)
updateScore s (Finished m (Series ms))      = Finished m (Series (ms |> s))
updateScore s (Scheduled Tie m)             = Finished m (Tieable s)
updateScore s (Scheduled NonTie m)          = Finished m (NonTieable s Nothing)
updateScore s (Scheduled Serie m)           = Finished m (Series (s :| []))
updateScore s m = m

removeScore :: Match -> Match
removeScore (Finished m (Tieable _))      = Scheduled Tie m
removeScore (Finished m (NonTieable _ _)) = Scheduled NonTie m
removeScore (Finished m (Series ms))      = Scheduled Serie m
removeScore m = m

sameMatch :: Match -> Match -> Bool
sameMatch a b = Data.Match.matchID a == Data.Match.matchID b

canOverwrite :: Match -> Match -> Bool
canOverwrite new old
    | sameMid && not sameT = True
    | otherwise = False
    where
        sameMid = Data.Match.matchID new == Data.Match.matchID old
        sameT = teamsInMatch new == teamsInMatch old


sameMatchList :: [Match] -> [Match] -> Bool
sameMatchList (x:xs) (y:ys)
    | sameMatch x y = sameMatchList xs ys
    | otherwise = False
sameMatchList [] [] = True
sameMatchList [] _ = False
sameMatchList _ [] = False


nextScheduled :: [Match] -> Maybe Match
nextScheduled ((Scheduled r m):ms) = Just (Scheduled r m)
nextScheduled (m:ms) = nextScheduled ms
nextScheduled [] = Nothing

firstFinished :: [Match] -> Maybe Match
firstFinished ((Finished m r):ms) = Just (Finished m r)
firstFinished (m:ms) = firstFinished ms
firstFinished [] = Nothing


lastMid :: NonEmpty Match -> MatchID
lastMid ms = incr $ NE.head $ NE.reverse $
             sort $ NE.map Data.Match.matchID ms
