module Data.Score
    ( findWinner
    --, Score.away
    --, Score.home
    --, Score.matchID
    , getPoints
    , getScore
    --, scoresAgainstTeam
    ) where

import Data.Types

import GHC.Generics (Generic)


-- Nothing means tie
findWinner :: Score -> Maybe Residence
findWinner (Scores hs as)
    | dif == GT = Just Home
    | dif == LT = Just Away
    | otherwise = Nothing
    where dif = compare hs as
findWinner (Default r)  = Just r

getPoints :: Residence -> Score -> Int
getPoints Home (Scores hs as)
    | hs > as = 2
    | hs == as = 1
    | otherwise = 0
getPoints Away (Scores hs as)
    | hs < as = 2
    | hs == as = 1
    | otherwise = 0
getPoints Home (Default Home) = 2
getPoints Home (Default Away) = 0
getPoints Away (Default Away) = 2
getPoints Away (Default Home) = 0


{-
-- FIXME: This has to be moved somewhere else
scoresAgainstTeam :: Roster -> Roster -> [Score] -> [Score]
scoresAgainstTeam team against = mapMaybe (filt team against)
    where
      filt team against res
          | h == team && a == against = Just res
          | a == team && h == against = Just res
          | otherwise = Nothing
          where
              h = Score.home res
              a = Score.away res
 -}

getScore :: Residence -> Score -> Maybe Int
getScore Home (Scores hs _) = Just hs
getScore Away (Scores _ as) = Just as
getScore team (Default _)   = Nothing
