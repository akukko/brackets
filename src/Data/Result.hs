{-# LANGUAGE OverloadedStrings #-}

module Data.Result
    ( findWinner
    , getPoints
    , getScore
    , result
    ) where

import Data.Types
import Data.List.NonEmpty as NE (NonEmpty(..))
import Helpers ((|>))
import qualified Data.Score as Score


result :: Res -> Score -> Result
result Tie s = Tieable s
result NonTie s = NonTieable s Nothing
result Serie s = Series (s :| [])

findWinner :: Result -> Maybe Residence
findWinner r = case r of
    NonTieable _ (Just s) -> Score.findWinner s
    NonTieable s Nothing -> Score.findWinner s
    Tieable s -> Score.findWinner s
    Series ms -> Nothing  -- FIXME: Implement best of Result winners.


getPoints :: Residence -> Result -> Int
getPoints residence r = case r of
    NonTieable _ (Just s) -> Score.getPoints residence s
    NonTieable s Nothing -> Score.getPoints residence s
    Tieable s -> Score.getPoints residence s
    Series ms -> 0

getScore :: Residence -> Result -> Maybe Int
getScore residence r = case r of
    NonTieable _ (Just s) -> Score.getScore residence s
    NonTieable s Nothing -> Score.getScore residence s
    Tieable s -> Score.getScore residence s
    Series ms -> Nothing



