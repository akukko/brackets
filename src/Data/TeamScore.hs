{-# LANGUAGE OverloadedStrings #-}

module Data.TeamScore
    ( TeamScore (..)
    , games
    , points
    , averageScore
    , bestGame
    , sortScores
    , sortByOverallAverage
    , Data.TeamScore.teamName
    ) where

import Data.Types as Types
import Helpers
import qualified Data.Score as Score

import Data.List
import Data.Text as Text ( Text, unpack, length, intercalate )
import Data.HashMap.Strict as HashMap ( HashMap, lookup )
import Data.Ord (comparing, Down(..))
import Data.Maybe

data TeamScore = TeamScore
        { team :: Roster
        , results :: [(Matchup, Score)]
        } deriving (Show, Eq)

-- This is used only for the comparison between teams from different groups
instance Ord TeamScore where
    compare ts1 ts2
      | avg /= EQ = avg
      | otherwise = best
      where avg = compare (averageScore ts2) (averageScore ts1)
            best = compare (bestGame ts2) (bestGame ts1)

teamName :: TeamScore -> Text
teamName TeamScore { team = Roster { rosterName = n } } = n

sortScores :: [TeamScore] -> [TeamScore]
sortScores ts = reverse $ concat $ decision criteria [ts]

sortByOverallPoints :: [TeamScore] -> [[TeamScore]]
sortByOverallPoints = sortAndGroup (map points)

sortByPeerToPeerPoints :: [TeamScore] -> [[TeamScore]]
sortByPeerToPeerPoints = sortAndGroup (peerToPeer sum getPoints)

sortByOverallAverage :: [TeamScore] -> [[TeamScore]]
sortByOverallAverage = sortAndGroup (map averageScore)

sortByPeerToPeerAverage :: [TeamScore] -> [[TeamScore]]
sortByPeerToPeerAverage = sortAndGroup (peerToPeer maybeAverage getScore)

sortByBestGame :: [TeamScore] -> [[TeamScore]]
sortByBestGame = sortAndGroup (map bestGame)

sortAndGroup :: Ord a1 => ([a2] -> [a1]) -> [a2] -> [[a2]]
sortAndGroup f ts = map snd $ groupBySnd $ sortOn (Down . snd) (zip ts (f ts))

maybeAverage xs = sum xs' `divide` Data.List.length xs'
    where xs' = catMaybes xs


getPoints :: Roster -> (Matchup, Score) -> Int
getPoints team (Matchup {home = h, away = a}, s)
    | team == h = Score.getPoints Home s
    | team == a = Score.getPoints Away s
    | otherwise = 0

getScore :: Roster -> (Matchup, Score) -> Maybe Int
getScore team (Matchup {home = h, away = a}, s)
    | team == h = Score.getScore Home s
    | team == a = Score.getScore Away s
    | otherwise = Nothing



-- FIXME: clean up peer to peer result calculation
peerToPeer :: ([b1] -> b2) -> (Roster -> (Matchup, Score) -> b1) -> [TeamScore] -> [b2]
peerToPeer f f' ts =
    map (\t -> f $ p2p (team t) (map team (filter (t/=) ts)) (results t)) ts
    where
        p2p team opponents results =
            concatMap (\x -> map (f' team) (scoresAgainstTeam team x results)) opponents

scoresAgainstTeam :: Roster -> Roster -> [(Matchup, Score)] -> [(Matchup, Score)]
scoresAgainstTeam team against = mapMaybe filt
    where
      filt (m, res)
          | h == team && a == against = Just (m, res)
          | a == team && h == against = Just (m, res)
          | otherwise = Nothing
          where
              h = Types.home m
              a = Types.away m

criteria :: [[TeamScore] -> [[TeamScore]]]
criteria = [ sortByOverallPoints
           , sortByPeerToPeerPoints
           , sortByOverallAverage
           , sortByPeerToPeerAverage
           , sortByBestGame
           ]

decision :: [[TeamScore] -> [[TeamScore]]] -> [[TeamScore]] -> [[TeamScore]]
decision (c:cs) [ts] = decision cs (c ts)
decision (c:cs) ts = concatMap (decision (tail criteria) . head criteria) ts
decision [] ts = ts  -- FIXME: what should be the final criteria if all else fails?

-- Getters for various statistics based on TeamScore

games :: TeamScore -> Int
games ts = Data.List.length (results ts)

points :: TeamScore -> Int
points ts = sum $ map (getPoints $ team ts) (results ts)

averageScore :: TeamScore -> Float
averageScore ts = average (mapMaybe (getScore $ team ts) (results ts))

bestGame :: TeamScore -> Int
bestGame ts = case results ts of
        [] -> 0
        res  -> maximum (mapMaybe (getScore $ team ts) res)
