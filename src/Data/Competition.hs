module Data.Competition
    ( addPlayoffsToCompetition
    , updateMatch
    , updateMatchWithID
    , generateTournament
    , tryGetMatch
    , addScore
    , deleteScore
    , getNextMatch
    , getLastMatch
    , updatePlayoffs
    , tryUpdateNextMatch
    , tryUpdateMatch
    , tryDeletePreviousScore
    )
    where

import Helpers
import Brackets
import Groups
import Scheduling
import Scoreboard
import Config
import Data.Types
import Data.Match as Match



import Data.List as List
import qualified Data.List.NonEmpty as NE
import Data.Text as Text (null, Text)



generateTournament :: CompetitionID -> GroupStageFormat -> [Text] -> Competition
generateTournament cid gsFmt ts = do
    -- let validNames = not $ any Text.null ts
    -- FIXME: Get real TeamIDs from the vault
    let indexTeams =  [1..(List.length ts)]
    let rosters = zipWith (\t tid -> Roster (TeamID tid) t []) ts indexTeams
    let groups = getGroups rosters gsFmt

    let schedule = getSchedule groups balancing (MatchID cid 0)
    Competition cid schedule []

latestMid :: Competition -> MatchID
latestMid Competition { groups = ((m:ms):gs), playoffs = p } =
    lastMid $ m NE.:| concat (ms:gs ++ p)
latestMid c = MatchID (competitionID c) 0

getNextMatch :: Competition -> Maybe Match
getNextMatch c = nextScheduled (concat (groups c) ++ concat (playoffs c))

getLastMatch :: Competition -> Maybe Match
getLastMatch c = firstFinished (reverse (concat (groups c) ++ concat (playoffs c)))

tryGetMatch mid c = List.find ((mid==) . Match.matchID) $
                        concat (groups c) ++ concat (playoffs c)

addPlayoffsToCompetition :: [[Match]] -> Competition -> Competition
addPlayoffsToCompetition p c = c { playoffs = p }

overwriteMatch :: Match -> [Match] -> [Match]
overwriteMatch s = replaceMatching (sameMatch s) s

tryOverwriteMatch :: Match -> [Match] -> [Match]
tryOverwriteMatch new = replaceMatching (canOverwrite new) new

tryUpdateNextMatch :: Score -> Competition -> Competition
tryUpdateNextMatch s c = maybe c (\m -> addScore s m c) $ getNextMatch c

tryDeletePreviousScore :: Competition -> Competition
tryDeletePreviousScore c = maybe c (`deleteScore` c) $ getLastMatch c

-- | Try to find a match where the teams match in the given order (home, away).
--If none or multiple matches are found, the competition remains unmodified.
tryUpdateMatch  :: Text -- ^ Home roster name
                -> Text -- ^ Away roster name
                -> Score -- ^ The score to be added to the match if found
                -> Competition -- ^ Competition to be modified
                -> Competition -- ^ Either original or modified competition
tryUpdateMatch hTeam aTeam s c =
    case filter (teamNamesMatch hTeam aTeam) $ concat (groups c ++ playoffs c) of
        [m] -> addScore s m c
        _  -> c -- FIXME: Handle multiple matching matches

updateMatchWithID :: MatchID -> Score -> Competition -> Competition
updateMatchWithID mid s c =
    case filter ((mid ==) . Match.matchID) $ concat (groups c ++ playoffs c) of
        [m] -> addScore s m c
        _  -> c -- FIXME: Handle multiple matching matches

addScore :: Score -> Match -> Competition -> Competition
addScore s m = updateMatch (updateScore s m)

deleteScore :: Match -> Competition -> Competition
deleteScore m = updateMatch (removeScore m)

updateMatch :: Match -> Competition -> Competition
updateMatch m c = updatePlayoffs $ c { groups = gs, playoffs = p }
    where
        gs = map (overwriteMatch m) (groups c)
        p = map (overwriteMatch m) (playoffs c)

firstRoundExists :: [[a]] -> Bool
firstRoundExists ((m:ms):rs) = True
firstRoundExists _ = False

updatePlayoffs :: Competition -> Competition
updatePlayoffs c = c { playoffs = refreshPlayoffs p (playoffsFromGroups gs)}
    where
        p = playoffs c
        gs = groups c

refreshPlayoffs :: [[Match]] -> [[Match]] -> [[Match]]
refreshPlayoffs (current:cs) (generated:gs)
    | identicalPairings current generated = current : refreshPlayoffs cs nextRounds
    | otherwise = updated : refreshPlayoffs cs nextRounds'
    where
        updated = foldl (flip tryOverwriteMatch) current generated
        (_:nextRounds) = getBrackets current
        (_:nextRounds') = getBrackets updated
refreshPlayoffs [] (generated:_) = generated : refreshPlayoffs [] nextRounds'
    where
        (_:nextRounds') = getBrackets generated
refreshPlayoffs _ _ = []

playoffsFromGroups :: [[Match]] -> [[Match]]
playoffsFromGroups ((m:ms):gs)
    | not $ all isFinished (concat ((m:ms):gs)) = []
    | otherwise = getBrackets firstRound
    where
        firstRound = getFirstRound (buildScoreboards ((m:ms):gs))
            (lastMid (m NE.:| (ms ++ concat gs)))
playoffsFromGroups _ = []

firstRoundSame :: Competition -> Bool
firstRoundSame c = fstRoundEqual (playoffs c) $ playoffsFromGroups (groups c)

playoffsMatch :: Competition -> Bool
playoffsMatch c = playoffPairingsMatch (playoffs c) $ playoffsFromGroups (groups c)

playoffPairingsMatch :: [[Match]] -> [[Match]] -> Bool
playoffPairingsMatch (x:xs) (y:ys) =
    identicalPairings x y && playoffPairingsMatch xs ys
playoffPairingsMatch [] [] = True
playoffPairingsMatch _ _ = False

fstRoundEqual :: [[Match]] -> [[Match]] -> Bool
fstRoundEqual (x:_) (y:_) = identicalPairings x y
fstRoundEqual _ _ = False

-- Check if the round is same.
identicalPairings :: [Match] -> [Match] -> Bool
identicalPairings (x:xs) (y:ys) = sameMatchup x y && identicalPairings xs ys
identicalPairings [] [] = True
identicalPairings _ _ = False
