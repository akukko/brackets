module Data.Matchup
    ( sameTeam
    , sameTeam3
    , sameTeam4
    , sameTeams
    , differentTeams
    ) where

import Data.Types
import Data.List

import GHC.Generics (Generic)


sameTeam :: Matchup -> Matchup -> Bool
sameTeam x y
    | away x == away y = True
    | away x == home y = True
    | home x == away y = True
    | home x == home y = True
    | otherwise = False

sameTeam3 :: Matchup -> Matchup -> Matchup -> Bool
sameTeam3 a b c = sameTeam a b || sameTeam a c || sameTeam b c

sameTeam4 :: Matchup -> Matchup -> Matchup -> Matchup -> Bool
sameTeam4 a b c d = sameTeam a b || sameTeam a c ||
                   sameTeam a d || sameTeam b c ||
                   sameTeam b d || sameTeam c d

sameTeams :: Matchup -> Matchup -> Bool
sameTeams x y
    | away x == away y && home x == home y = True
    | away x == home y && home x == away y = True
    | home x == away y && away x == home y = True
    | home x == home y && away x == away y = True
    | otherwise = False


differentTeams :: Matchup -> Matchup -> Bool
differentTeams x y = length (nub [home x, away x, home y, away y]) == 4
