module Balance (
    balanceMatches) where

import Data.Matchup
import Data.Types
import Config
import Helpers



balanceMatches :: Balancing -> [[Matchup]] -> [[Matchup]]
balanceMatches (League _ _) ms = [balanceConcurrentTwo (concat ms)]
balanceMatches b ms            = map (balanceGroupMatches b) ms

balanceGroupMatches :: Balancing -> [Matchup] -> [Matchup]
balanceGroupMatches _ [a, b] = [a, b]     -- With 2 and 3 matchups
balanceGroupMatches _ [a, b, c] = [a, b, c]  -- the order doesn't matter
balanceGroupMatches ConcurrentFour ms = balanceConcurrentFour ms
balanceGroupMatches ConcurrentTwo ms = balanceConcurrentTwo ms
balanceGroupMatches Sparse ms = balanceSparse ms
balanceGroupMatches NoBalancing ms = ms
balanceGroupMatches (League _ _) ms = ms


balanceConcurrentTwo :: [Matchup] -> [Matchup]
balanceConcurrentTwo ms = concurrent ms [] []

balanceSparse :: [Matchup] -> [Matchup]
balanceSparse ms = sparse ms [] []

balanceConcurrentFour :: [Matchup] -> [Matchup]
balanceConcurrentFour ms
    | mod (length ms) 28 == 0 = pro ms [] []
    | otherwise = concurrent ms [] []

{-

Concurrent playing order. Order the games so that two games can be played
simultaneously. Used for larger groups to save time.

-}

concurrent :: [Matchup] -> [Matchup] -> [Matchup] -> [Matchup]
concurrent (f:s:ms) (fb:sb:bs) r
    | not $ sameTeam fb sb = concurrent (f:s:ms) bs (r++fb:[sb])
    | not $ sameTeam f fb = concurrent ms (s:sb:bs) (r++f:[fb])
    | not $ sameTeam f sb = concurrent ms (s:fb:bs) (r++f:[sb])
    | not $ sameTeam s fb = concurrent ms (f:sb:bs) (r++s:[fb])
    | not $ sameTeam s sb = concurrent ms (f:fb:bs) (r++s:[sb])
    | not $ sameTeam f s = concurrent ms (fb:sb:bs) (r++f:[s])
    | otherwise = concurrent ms (s:f:fb:sb:bs) r
concurrent [] (fb:sb:bs) r
    | sameTeam fb sb = concurrent (reverse (r++bs++fb:[sb])) [] []
    | otherwise = concurrent [] bs (r++fb:[sb])
concurrent (f:s:ms) [] r
    | sameTeam f s = concurrent ms (f:[s]) r
    | otherwise = concurrent ms [] (r++f:[s])
concurrent (m:ms) (b:bs) r = concurrent ms (m:b:bs) r
concurrent [] [b] r = r++[b]
concurrent [m] [] r = r++[m]
concurrent [] [] r = r


{-

Sparse playing order. Don't make three consecutive matchups.

Build a new list of matchups, and if the same team appears three times in a
row, add that matchup to buffer. At the beginning of each iteration, try to
empty the buffer. If the result is not three games for the same team in a
row, add the first matchup in the buffer to the end of the matchup list.

-}

sparse :: [Matchup] -> [Matchup] -> [Matchup] -> [Matchup]
sparse (m:ms) (b:bs) r
    | not $ sameTeamOccurs (b:r) 3 = sparse (m:ms) bs (b:r)
    | not $ sameTeamOccurs (m:r) 3 = sparse ms (b:bs) (m:r)
    | otherwise = sparse ms (m:b:bs) r
sparse (m:ms) [] r
    | not $ sameTeamOccurs (m:r) 3 = sparse ms [] (m:r)
    | otherwise = sparse ms [m] r
sparse [] (b:bs) r
    | not $ sameTeamOccurs (b:r) 3 = sparse [] bs (b:r)
    | otherwise = sparse (reverse (bs++r++[b])) [] []
sparse [] [] r = r


{-

Order the games so that they can be played on four different
fields simultaneously. This function is very peculiar about its inputs,
it will most likely get stuck with many inputs.

-}



pro :: [Matchup] -> [Matchup] -> [Matchup] -> [Matchup]
pro (a:b:ms) [] r
    | not $ sameTeam a b = pro ms (a:[b]) r
    | otherwise = pro (ms++[b]) [a] r
pro (a:ms) [x] r
    | not $ sameTeam a x = pro ms (a:[x]) r
    | otherwise = pro (ms++[a]) [x] r
pro (a:ms) (x:y:z:rs) r
    | not $ sameTeam4 a x y z = pro ms [] (x:y:z:a:r)
    | otherwise = pro (ms++[a]) (x:y:[z]) r
pro (a:ms) (x:y:rs) r
    | not $ sameTeam3 a x y = pro ms (a:x:[y]) r
    | otherwise = pro (ms++[a]) (x:[y]) r
pro [] _ r = r
pro _ _ r = []

sameTeamOccurs :: [Matchup] -> Int -> Bool
sameTeamOccurs ms i = same (take i ms) i []

same :: [Matchup] -> Int -> [Roster] -> Bool
same (m:ms) i s = same ms i (home m:away m:s)
same [] i s = numIsMore s i s



