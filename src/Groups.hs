module Groups
    ( calculateGroupAmount
    , getGroups
    , splitToNGroups
    ) where

import Config
import Helpers


import Data.List
import Data.Ord
import Data.Maybe

getGroups :: [a] -> GroupStageFormat -> [[a]]
-- Fills the groups in such a way that the group stage doesn't
--take too long (6 rounds of team games ≈ 3 hours) but teams
--get maximum amount of games.
getGroups teams (ReadyGroupsOf n) = chunks n teams
getGroups teams TwelveFields = maxGamesWithinLimits teams 12 6
getGroups teams TenFields = maxGamesWithinLimits teams 10 6
getGroups teams SingleRoundRobin = [teams]
getGroups teams ThreeGroups = splitToNGroups 3 teams

{- | Returns the group configuration for given teams that obeys the round limit
and produces maximum number of games. If no such configuration exists, returns
an empty list.
-}
maxGamesWithinLimits :: [a] -> Int -> Integer -> [[a]]
maxGamesWithinLimits teams fields roundLimit = do
    let opts = groupOptions teams fields
    case mapMaybe (validFieldOptions roundLimit fields) opts of
        (x:_) -> x
        _ -> []

validFieldOptions ::  Integer -> Int -> [[a]] -> Maybe [[a]]
validFieldOptions roundLimit fields grp = case validOptions of
        Just (_:_) -> Just grp
        _ -> Nothing
    where
        -- Gather all permutations of field amounts for groups
        options = (`fieldOptions` []) <$> fieldUsages roundLimit grp fields
        -- Check that the total amount of fields needed for
        -- a configuration isn't over the limit
        validOptions = filter (\x -> sum x <= fields) <$> options

-- | Return a list of possible field amounts for each group.

--      groups ---+   accumulator   +--- possible field amounts for
--                |        |        |    given group configuration
fieldOptions :: [[a]] -> [[a]] -> [[a]]
fieldOptions [] a = a
-- Don't try to map empty a, instead initialize a to contain x elements
fieldOptions (x:xs) [] = fieldOptions xs $ map (: []) x
fieldOptions (x:xs) a' = fieldOptions xs $ concatMap (fieldOptionsForGroup x) a'
    where
        fieldOptionsForGroup :: [a] -> [a] -> [[a]]
        fieldOptionsForGroup [] a = [a]
        fieldOptionsForGroup ys a = map (\y -> a ++ [y]) ys


groupOptions :: [a] -> Int -> [[[a]]]
groupOptions teams fields = filter (all (\g -> length g > 1)) $
                            map (`splitToNGroups` teams) [1..fields]

matchesInGroupOfSize :: (Integral p, Integral a2) => a2 -> p
matchesInGroupOfSize 0 = 0
matchesInGroupOfSize 1 = 0
matchesInGroupOfSize n = round $ (n * (n - 1)) `divide` 2

fieldUsages roundLimit (g:gs) fields =
    (\x -> (:) x <$> rest) =<< fieldUsage roundLimit g fields []
    where
        rest = fieldUsages roundLimit gs fields

fieldUsages roundLimit [] fields = Just []




fieldUsage roundLimit teams 0 (a:as) = Just (a:as)
fieldUsage roundLimit teams 0 [] = Nothing
fieldUsage roundLimit teams fields a
    --  | not obeysRoundLimit = trace ((show roundLimit) ++ " " ++ show teams ++ " " ++show fields ++ " ") Nothing
    | divisible && obeysRoundLimit = fieldUsage roundLimit teams (fields - 1) (fields : a)
    | otherwise = fieldUsage roundLimit teams (fields - 1) a
    where
        divisible = divide (length teams) fields >= 2
        obeysRoundLimit = rounds teams fields <= roundLimit


fieldNeed teams fields
    | divide (length teams) fields >= 2 = fields
    | otherwise = fieldNeed teams (fields - 1)

rounds teams fields
    | divide (length teams) fields >= 2 =
          ceiling $ matchesInGroupOfSize (length teams) `divide` fields
    | otherwise = rounds teams (fields - 1)

groupAmounts :: [Int]
groupAmounts = iterate (*2) 1

calculateGroupAmount :: Int -> Int -> Maybe Int
calculateGroupAmount teams maxPerGroup
    | maxPerGroup <= 0 = Nothing
    | amount == 0 = Nothing
    | reminder == 0 = find (>=amount) groupAmounts
    | otherwise = find (>amount) groupAmounts
    where
        (amount, reminder) = quotRem teams maxPerGroup

{- | Split teams into groups based on their seeding.

For example 9 teams split into 3 groups:

Group 1:    1 6 7
Group 2:    2 5 8
Group 3:    3 4 9

Note that:

>>> splitToNGroups 2 [1,2,3,4,5,6,7]
[[1,4,5,7], [2,3,6]]

The last team (7) is placed in the first group, because
it produces groups with more even distribution of seeds.

>>> average [1,4,5,7] - average [2,3,6]
0.5833333333333335

>>> average [2,3,6,7] - average [1,4,5]
1.1666666666666665

It's also good that the first seed gets a group with higher seed average,
i.e. theoretically easier group. However, in practice the group isn't
necessarily easier because team strength is not linear like the seeding.

It can also be argued that adding team 7 to the first group doesn't
make it any easier since all teams are assumed to beat team 7.
-}
splitToNGroups :: Int -> [a] -> [[a]]
splitToNGroups n teams =
    transpose $                         -- [[1,6,7], [2,5,8], [3,4,9]]
    zipWith ($) (cycle [id, reverse]) $ -- [[1,2,3], [6,5,4], [7,8,9]]
    chunks                              -- [[1,2,3], [4,5,6], [7,8,9]]
    n                                   -- 3
    teams                               -- [1,2,3,4,5,6,7,8,9]

