{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}


module IO.FetchLeagueResults
    ( readLeagueResults
    , fetchLeagueResults
    , fetchPlayerStats
    ) where

import Data.Types
import Data.Match
import Helpers
import Config

import qualified Data.Text as T


import Text.HTML.Scalpel

import Data.List
import Text.Read
import Data.Maybe
import qualified Data.HashMap.Strict as HashMap (HashMap, insert, empty, lookup, toList)
import qualified Data.List.NonEmpty as NE (map, nonEmpty)






toThrow :: String -> Maybe Throw
toThrow "E" = Just Unused
toThrow "H" = Just Hauki
toThrow x = Points <$> (readMaybe x :: Maybe Int)

links :: URL -> IO (Maybe [String])
links url = scrapeURL url $ chroots "td" $ attr "href" "a"


toMatch a b Nothing       =
    Scheduled Tie (Matchup makeMid (roster a) (roster b))
toMatch a b (Just (x, y)) =
    Finished (Matchup makeMid (roster a) (roster b)) (Tieable $ Scores x y)

toPlayoffMatch a b Nothing     =
    Scheduled Tie (Matchup makeMid (roster a) (roster b))
toPlayoffMatch a b (Just (x, y)) =
    Finished (Matchup makeMid (roster a) (roster b)) (NonTieable (Scores x y) Nothing)

awayForfeits :: URL -> IO (Maybe [Maybe (T.Text, T.Text)])
awayForfeits url = scrapeURL url $ chroots "tr" $ inSerial $ do
    t1 <- seekNext $ text ("td" @: [hasClass "voittaja", hasClass "kotijoukkue"])
    t2 <- seekNext $ text "a"
    location <- seekNext $ text "td"
    score <- seekNext $ text "td"
    return $ case T.strip score of
        "Luovutusvoitto" -> Just (T.strip t1, T.strip t2)
        _                -> Nothing

homeForfeits :: URL -> IO (Maybe [Maybe (T.Text, T.Text)])
homeForfeits url = scrapeURL url $ chroots "tr" $ inSerial $ do
    t1 <- seekNext $ text "a"
    t2 <- seekNext $ text ("td" @: [hasClass "voittaja", hasClass "vierasjoukkue"])
    location <- seekNext $ text "td"
    score <- seekNext $ text "td"

    return $ case T.strip score of
        "Luovutusvoitto" -> Just (T.strip t1, T.strip t2)
        _                -> Nothing

matchesFromPage :: URL -> IO (Maybe [Match])
matchesFromPage url = (map (uncurry3 toMatch) <$>) <$> resultsFromPage url


resultsFromPage :: URL -> IO (Maybe [(T.Text, T.Text, Maybe (Int, Int))])
resultsFromPage url = scrapeURL url $ chroots "tr" $ inSerial $ do
    t1 <- seekNext $ text "a"
    t2 <- seekNext $ text "a"
    location <- seekNext $ text "td"
    score <- seekNext $ text "td"
    return (T.strip t1, T.strip t2, separateScores score)



setResultsFromPage :: URL -> IO (Maybe [Maybe (Int, Int)])
setResultsFromPage url = scrapeURL url $ chroots ("tr" @: [hasClass "eratulos"]) $ inSerial $ do
        resText     <- seekNext $ text "td"
        t1          <- seekNext $ text "td"
        setResult1  <- seekNext $ text "td"
        line        <- seekNext $ text "td"
        setResult2  <- seekNext $ text "td"
        return $ readScore (setResult1, setResult2)


playoffsFromPage :: URL -> IO (Maybe [[[(Maybe (T.Text, T.Text), Maybe (Int, Int))]]])
playoffsFromPage url = scrapeURL url $ chroots ("div" @: [hasClass "tilastot"]) $ do
    chroots "table" $ do
        chroots "tr" $ inSerial $ do
            desc <- seekNext $ text "td"
            teams <- seekNext $ text "td"
            a <- seekNext $ texts "a"
            score <- seekNext $ text "td"
            return (separateNames teams, separateScores score)


separateScores :: T.Text -> Maybe (Int, Int)
separateScores s = exactlyTwo $ mapM ((\x -> readMaybe x :: Maybe Int) . T.unpack . T.strip) (T.splitOn "–" s)

readScore :: (T.Text, T.Text) -> Maybe (Int, Int)
readScore (s1, s2) = func (rd s1) (rd s2)
    where
        func (Just a) (Just b) = Just (a, b)
        func _ _ = Nothing

        rd = (\x -> readMaybe x :: Maybe Int) . T.unpack . T.strip

separateNames :: T.Text -> Maybe (T.Text, T.Text)
separateNames s = exactlyTwo $ Just (map T.strip (T.splitOn "–" s))

exactlyTwoMaybes :: Maybe [Maybe b] -> Maybe (b, b)
exactlyTwoMaybes (Just [Just a, Just b]) = Just (a, b)
exactlyTwoMaybes _ = Nothing

exactlyTwo :: Maybe [b] -> Maybe (b, b)
exactlyTwo (Just [a, b]) = Just (a, b)
exactlyTwo _ = Nothing


statsIds :: Functor f => f [String] -> f [String]
statsIds ls = mapMaybe (stripPrefix "tilastot/ottelu/") <$> ls

playersFromPage :: URL -> IO (Maybe [String])
playersFromPage url = scrapeURL url $ chroots "tr" $ text ("td" @: [hasClass "pelaaja"])

statsFromPage :: URL -> IO (Maybe [String])
statsFromPage url = scrapeURL url $ chroot "table" $ texts ("td" @: [hasClass "heitto"])

maybeThrows :: [String] -> Maybe [Throw]
maybeThrows (s:ss) = case toThrow s of
        Just t  -> (:) t <$> maybeThrows ss
        Nothing -> Nothing
maybeThrows [] =  Just []

trimNames :: [String] -> Maybe [T.Text]
trimNames (s:ss) = case (T.strip . T.pack) . flip take s <$> elemIndex ',' s of
        Just n  -> (:) n <$> trimNames ss
        Nothing -> Nothing
trimNames [] = Just []


getStats :: URL -> IO (Maybe (((Int, Int), (Int, Int)), [PlayerStats]))
getStats url = do
    plrs <- (trimNames =<<) <$> playersFromPage url
    stats <- (maybeThrows =<<) <$> statsFromPage url
    results <- exactlyTwoMaybes <$> setResultsFromPage url
    return $ case (plrs, stats, results) of
        (Just p, Just s, Just r) -> (r,) <$> makePlayers p s
        _                -> Nothing


makePlayers :: [T.Text] -> [Throw] -> Maybe [PlayerStats]
makePlayers (p:ps) (a:b:c:d:ts)= (:) (PlayerStats p a b c d) <$> makePlayers ps ts
makePlayers [] []  = Just []
makePlayers a b  = error "Weird amount of throws or players or set results"

buildPlayoffSeries :: [[[(Maybe (T.Text, T.Text), Maybe (Int, Int))]]] -> [[[Match]]]
buildPlayoffSeries = map (map $ playoffMatch HashMap.empty)


playoffMatch :: HashMap.HashMap (T.Text, T.Text) [(Int, Int)]
    -> [(Maybe (T.Text, T.Text), Maybe (Int, Int))]
    -> [Match]
playoffMatch acc ((Just (t1, t2), Just (s1, s2)):ms) = case HashMap.lookup (t1, t2) acc of
    Just rs -> playoffMatch (HashMap.insert (t1, t2) (rs ++ [(s1, s2)]) acc) ms
    Nothing -> case HashMap.lookup (t2, t1) acc of
        Just rs -> playoffMatch (HashMap.insert (t2, t1) (rs ++ [(s2, s1)]) acc) ms
        Nothing -> playoffMatch (HashMap.insert (t2, t1) [(s2, s1)] acc) ms

playoffMatch acc ((Just (t1, t2), Nothing):ms) = case HashMap.lookup (t1, t2) acc of
    Just rs -> playoffMatch acc ms
    Nothing -> case HashMap.lookup (t2, t1) acc of
        Just rs -> playoffMatch acc ms
        Nothing -> playoffMatch (HashMap.insert (t2, t1) [] acc) ms
playoffMatch acc ((_, _):ms) = playoffMatch acc ms

playoffMatch acc [] = map (uncurry buildScoreList) $ HashMap.toList acc


buildScoreList :: (T.Text, T.Text) -> [(Int, Int)] -> Match
buildScoreList (t1, t2) ss = case NE.nonEmpty ss of
    Just ne -> Finished
                (Matchup makeMid (roster t1) (roster t2))
                (Series (NE.map (uncurry Scores) ne))
    Nothing -> Scheduled Tie $ Matchup makeMid (roster t1) (roster t2)


teamStats :: [(((Int, Int), (Int, Int)), [PlayerStats])] -> [(TeamStats, TeamStats)]
teamStats
    ( (((hs1, as1), (hs2, as2))
    , [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p]):ps) =
        (TeamStats (SetStats hs1 a b e f)
        ( SetStats hs2 k l o p)
        , TeamStats (SetStats as1 c d g h) (SetStats as2 i j m n)
        ) : teamStats ps

teamStats [] = []
teamStats (stats:ss) = error $ "Weird amount of stats (stats:ss): " ++ show stats ++ ": " ++ show ss

fetchPlayerStats :: IO ([(TeamStats, TeamStats)], [(TeamStats, TeamStats)])
fetchPlayerStats = do
    let resultPage = "https://kyykkaliiga.fi/ottelut-liiga"
    ls <- links resultPage
    let sIds = fromMaybe [] $ statsIds ls
    let sUrls = fmap ("https://kyykkaliiga.fi/tilastot/ottelu/" ++) sIds

    (_, maybePlayoffs) <- fetchLeagueResults

    let (regular, playoffs) = case maybePlayoffs of
            Just po -> do
                let playoffAmt = sum . map gameAmount $ concat po
                let regularAmt = length sUrls - playoffAmt

                splitAt regularAmt sUrls
            Nothing -> (sUrls, [])

    let stats xs = teamStats . catMaybes <$> mapM getStats xs


    reg <- stats regular
    poffs <- stats playoffs
    return (reg, poffs)



fetchLeagueResults :: IO (Maybe [Match], Maybe [[Match]])
fetchLeagueResults = do
    let resultPage = "https://kyykkaliiga.fi/ottelut-liiga"

    allResults <- matchesFromPage resultPage

    h <- (map (\(a,b) ->
                   Finished (Matchup makeMid (roster a) (roster b))
                   (Tieable $ Default Away)) . catMaybes <$>)
                        <$> homeForfeits resultPage
    a <- (map (\(a,b) ->
                   Finished (Matchup makeMid (roster a) (roster b))
                   (Tieable $ Default Home)) . catMaybes <$>)
                        <$> awayForfeits resultPage

    maybePlayoffs <-
        playoffsFromPage "https://kyykkaliiga.fi/sarjataulukot-pudotuspelit"
    let regular = case maybePlayoffs of
            Just po -> reverse . drop (length (concat $ head po)) . reverse <$> allResults
            Nothing -> allResults

    let allR = fmap (++) (fmap (++) h <*> a) <*> regular
    return (allR, head . buildPlayoffSeries <$> maybePlayoffs)


readLeagueResults s = map (parse . map strip . wordsWhen (=='\t')) (lines s)

makeMid = MatchID Config.competitionId 0

roster name = Roster (TeamID 1) name []

strip = T.unpack . T.strip . T.pack

parse [date, time, fstTeam, sndTeam, place, result, link]
    | null result = Scheduled Tie -- FIXME: Is Tieable always correct here?
                    (Matchup makeMid (roster (T.pack fstTeam)) (roster (T.pack sndTeam)))
    | otherwise   = Finished
                    (Matchup makeMid (roster (T.pack fstTeam)) (roster (T.pack sndTeam)))
                     (Tieable $ Scores res1 res2)
    where
        (res1, res2) = parseResult result
parse _ = error "Results file has wrong format."

parseResult res = (read x :: Int, read y :: Int)
    where
        [x, y] = wordsWhen (=='–') res
