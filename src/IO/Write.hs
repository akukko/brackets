{-# LANGUAGE OverloadedStrings #-}

module IO.Write ( writeSchedule
                , writeTeams
                ) where

import Data.Types as Types
import Data.List
import qualified Data.Text as Text ( intercalate, Text )
import qualified Data.Text.IO as TextIO ( writeFile )

writeSchedule :: FilePath -> [[Match]] -> IO ()
writeSchedule path rs = writeFile path (intercalate "\n" (map (writeGroup "") rs))

writeGroup :: String -> [Match] -> String
writeGroup = foldl buildLine

-- FIXME: Figure out tiebreakers
buildLine :: String -> Match -> String
buildLine cur match = cur ++ txt match
    where
        d = '|'
        h m = show $ Types.home m
        a m = show $ Types.away m
        mid m = show $ Types.matchID m

        scoreTexts (Scores hs as) = (show hs, show as)
        scoreTexts (Default Home) = ("Default", "")
        scoreTexts (Default Away) = ("", "Default")


        -- FIXME: handle tiebreakers and series better
        resTexts (Tieable s)         = scoreTexts s
        resTexts (NonTieable s _) = scoreTexts s
        resTexts (Series _)          = ("", "")

        txt (Scheduled _ m) =
            replicate 2 d ++ h m ++ [d] ++ a m ++ [d] ++ mid m ++ "\n"
        txt (Finished m r) =
            let (hs, as) = resTexts r in
                (hs ++ [d] ++ as ++ [d] ++ h m ++ [d] ++ a m ++ [d] ++ mid m ++ "\n")
        txt (HomeOnly mid' h') =
            replicate 2 d ++ show h' ++ [d] ++ [d] ++ show mid' ++ "\n"
        txt (AwayOnly mid' a') =
            replicate 3 d ++ show a' ++ [d] ++ show mid' ++ "\n"
        txt (Unscheduled mid') =
            replicate 4 d ++ show mid' ++ "\n"


getScore :: Maybe Int -> String
getScore = maybe "" show

writeTeams :: String -> [[Text.Text]] -> IO ()
writeTeams path ts = TextIO.writeFile path $ inter (map inter ts)
    where
        inter = Text.intercalate "\n"
