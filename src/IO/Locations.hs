module IO.Locations
    ( userDataDir
    , userData
    , groupStageFile
    , teamsFile
    , playoffFile
    , vaultFile
    ) where

userDataDir :: FilePath
userDataDir = "data/"

userData :: FilePath -> FilePath
userData s = userDataDir ++ s ++ ".txt"

groupStageFile :: FilePath
groupStageFile = userData "groupStage"

teamsFile :: FilePath
teamsFile = userData "teams"

playoffFile :: FilePath
playoffFile = userData "playoffs"

vaultFile :: FilePath
vaultFile = userDataDir ++ "vault"
