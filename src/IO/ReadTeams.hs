module IO.ReadTeams
    ( readTeams
    , readGroups
    ) where

import Data.Char
import Data.Text as Text ( Text, pack)

readTeams :: String -> [Text]
readTeams t = readTeamsRec (Prelude.lines t) []

readTeamsRec :: [String] -> [Text] -> [Text]
readTeamsRec (x:xs) a
    | null (dropWhile isSpace x) = readTeamsRec xs a -- ignore empty lines
    | head x == '#' = readTeamsRec xs a
    | head x == '!' = a
    | otherwise = readTeamsRec xs (a++[pack x])
readTeamsRec [] a = a

readGroups :: String -> [[Text]]
readGroups t = readGroupsRec (Prelude.lines t) [] [] False


readGroupsRec :: [String] -> [[Text]] -> [Text] -> Bool -> [[Text]]
readGroupsRec (x:xs) gs a empty
    | null (dropWhile isSpace x) && empty = readGroupsRec xs gs [] True
    | null (dropWhile isSpace x) = readGroupsRec xs (gs++[a]) [] True
    | head x == '#' = readGroupsRec xs gs a empty
    | head x == '!' = [a]
    | otherwise = readGroupsRec xs gs (a++[pack x]) False
readGroupsRec [] gs a _ = gs++[a]