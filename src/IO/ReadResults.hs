module IO.ReadResults
    ( readResults
    , splitOnEmptyLine
    ) where


import Data.Types as Types
import Helpers


import Data.Char
import Data.Text (unpack, pack, strip)
import Data.List
import Prelude
import Text.Read


sep :: Char
sep = '|'

readResults :: String -> [[Match]]
readResults s = splitOnEmptyLine (lines s) [] []

splitOnEmptyLine :: [String] -> [Match] -> [[Match]] -> [[Match]]
splitOnEmptyLine (s:ss) c r
    | not (null s) = splitOnEmptyLine ss (c++[parse s]) r
    | otherwise = splitOnEmptyLine ss [] (r++[c])
splitOnEmptyLine [] c r = r++[c]

parse :: String -> Match
parse s = parseResult $ wordsWhen (==sep) s

parseResult :: [String] -> Match
parseResult s
    | length s > 4 = case (ri (head s), ri (s!!1), rt (s!!2), rt (s!!3), rm (s!!4)) of
        (Just (Just hs), Just (Just as), Just h, Just a, Just mid)
                                                       ->
            Finished (Matchup mid h a) (Tieable $ Scores hs as)
        (Just Nothing, _, Just h, Just a, Just mid)    ->
            Finished (Matchup mid h a) (Tieable $ Default Home)
        (_, Just Nothing, Just h, Just a, Just mid)    ->
            Finished (Matchup mid h a) (Tieable $ Default Away)
        (Nothing, Nothing, Just h, Just a, Just mid)   ->
            Scheduled Tie $ Matchup mid h a
        (Nothing, Nothing, Just h, Nothing, Just mid)  ->
            HomeOnly mid h
        (Nothing, Nothing, Nothing, Just a, Just mid)  ->
            AwayOnly mid a
        (Nothing, Nothing, Nothing, Nothing, Just mid) ->
            Unscheduled mid
        -- FIXME: Rework this error scenario to return Maybe or Either and handle those accordingly.
        _ -> error $ "Impossible situation encountered when reading schedulefile: " ++ intercalate ", " s ++ "\n"
    | otherwise = error "Wrong amount of cells in the result file."

rt :: String -> Maybe Roster
rt = rd

rm :: String -> Maybe MatchID
rm = rd

ri :: String -> Maybe (Maybe Int)
ri "Default" = Just Nothing
ri i = rd i

rd :: Read a => String -> Maybe a
rd s
    -- FIXME: This probably checks that the line isn't null. Make sure this is true.
    | not (null (dropWhile isSpace s)) = readMaybe (unpack (strip $ pack s))
    | otherwise = Nothing

