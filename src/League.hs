module League
    ( buildPairings
    , buildSchedules
    ) where


import Helpers

import Data.List
import Data.Ord as Ord

data Block a
    = Four (a, a) (a, a)
    | Three (a, a) a
    | Two (a, a)
    deriving (Show, Eq, Ord)


firstPair (Four a _) = a
firstPair (Three a _) = a
firstPair (Two a) = a


four :: Block a -> Bool
four (Four _ _) = True
four _ = False

two :: Block a -> Bool
two (Two _) = True
two _ = False


pairs (Four a b) = [a, b]
pairs (Three a b) = [a]
pairs (Two a) = [a]

teams :: Block a -> [a]
teams (Four (a, b) (c, d)) = [a, b, c, d]
teams (Three (a, b) c) = [a, b, c]
teams (Two (a, b)) = [a, b]

multipleLongDays :: Eq a => [Block a] -> [[Block a]] -> Bool
multipleLongDays bs = any (\e -> any (`teamInAllBlocks` e) longDayPairs)
    where
        longDayPairs = filter (`teamInAllBlocks` bs) (concatMap pairs bs)

teamInAllBlocks :: Eq a => (a,a) -> [Block a] -> Bool
teamInAllBlocks t = all (elem t . pairs)

matchAmount :: Num p => Block a -> p
matchAmount (Four _ _) = 2
matchAmount (Three _ _) = 2
matchAmount (Two _) = 1


blocksMatch :: Eq a => Block a -> Block a -> Bool
blocksMatch (Four p1 p2) (Four p3 p4) = p1 == p3 || p1 == p4 || p2 == p3 || p2 == p4
blocksMatch (Four p1 p2) (Two p3) = p1 == p3 || p2 == p3
blocksMatch (Two p3) (Four p1 p2) = p1 == p3 || p2 == p3
blocksMatch (Three p1 t3) (Four p2 p3) = p1 == p2 || p1 == p3
blocksMatch (Four p2 p3) (Three p1 t3) = p1 == p2 || p1 == p3
blocksMatch _ _ = False


buildPairings :: (Num a, Enum a) => a -> [Block a]
buildPairings teamAmt = helper [1..teamAmt] []



helper :: [a] -> [Block a] -> [Block a]
helper (a:b:ts) pairings = helper ts (pairings++new++[Two (a, b)])
    where
        new = map func $ chunks 2 ts
        func [x,y] = Four (a, b) (x, y)
        func [x] = Three (a, b) x
        func _ = error "Can't split into blocks."

helper _ pairings = pairings

buildSchedules _ _ [] = []
buildSchedules min' max' blocks = map (add3s threes threes . add2sWrapper twos) fourBlockSchedules
    where
        (fours, twosAnd3s) = partition four blocks
        (twos, threes) = partition two twosAnd3s

        divs = findPossibleDayDivisions min' max' (length fours)
        fourBlockSchedules = validate (arrangements fours) divs divs


add2sWrapper ts ss = {-debug ts debug ss $-} res
    where
        res = add2s ts ts ss

add2s [] [] ss = ss
add2s o (t:ts) (s:ss) = case add t s of
        Just added  -> added : add2s o ts ss
        Nothing -> add2s o ts (s:ss)
add2s orig [] ss = add2s orig orig ss
add2s _ _ _ = []


add3s [] [] ss = ss
add3s orig (three:ts) (s:ss) = case add three s of
        Just added -> added : add3s orig ts ss
        Nothing -> add3s orig ts (s:ss)
add3s orig [] ts = add3s orig orig ts
add3s _ _ _ = []

add t (a:b:ss)
    | inFstNotSec t a b = Just $ t : (a:b:ss)
    | inFstNotSec t a' b' = Just $ reverse $ t : (a':b':ss')
    | otherwise = Nothing
    where
        (a':b':ss') = reverse (a:b:ss)
add t [a]
    | inPair t a = Just $ t : [a]
    | otherwise = Nothing
add t [] = Nothing

inFstNotSec x a b = inPair x a && not (inPair x b) && four a
-- firstPair can be used because the pairs are always the same (1, 2 are together, 3, 4 etc.)
inPair a b = firstPair a `elem` pairs b

validateMatchday (a:b:bs) existing = (:) <$> validateBlock a b bs existing <*> validateMatchday (b:bs) existing
validateMatchday x _ = Just x

{-|
    Check that no team has multiple days where they play four games in a row.
    Also check that one team doesn't have a break between their games
-}
validateBlock a b bs exist =
    if blocksMatch a b
        && not (multipleLongDays [a, b] exist)
        && not (samePairs a bs)
        then Just a
        else Nothing


samePairs a bs = any (`elem` concatMap pairs bs) (pairs a)

validate (x:xs) (d:ds) o = case splitAccordingTo [] d x of
        Just split -> split : validate (x:xs) ds o
        Nothing -> validate (x:xs) ds o
validate (x:xs) [] origDivs = validate xs origDivs origDivs
validate [] _ _ = []


splitAccordingTo acc (s:splits) xs = let (h, t) = splitAt s xs in
    (\x -> splitAccordingTo (x : acc) splits t) =<< validateMatchday h acc
splitAccordingTo acc _ _ = Just acc


findPossibleDayDivisions :: Int -> Int -> Int -> [[Int]]
findPossibleDayDivisions min' max' len = listsSumLen min' max' len (div len 2)

listsSumLen _ _ s 0 = [[] | s == 0]  -- FIXME: Figure out what this does
listsSumLen min' max' s l = [ n0:ns | n0 <- [min'..(Ord.min max' s)], ns <- listsSumLen min' max' (s-n0) (l-1) ]

arrs :: Eq a => Int -> [(a, a)] -> [(a, a)] -> [[(a, a)]]
arrs _ _ [] = []
arrs len x [y] = [x++[y] | length (x++[y]) == len]
arrs len x (y:ys) = arrs len xy (notIn xy ys) ++ arrs len x ys
    where xy = x++[y]

notIn :: (Foldable t, Eq a) => t (a, a) -> [(a, a)] -> [(a, a)]
notIn checkable = filter (\x -> not (any (isInTuple x) checkable))

isInTuple :: Eq a => (a, a) -> (a, a) -> Bool
isInTuple (a,b) (c,d) = a == c || a == d || b == c || b == d

arrangements :: Eq a => [a] -> [[a]]
arrangements blocks
    | even len = arrangs blocks
    | otherwise = map (++ [last blocks]) $ arrangs $ init blocks
    where
        len = length blocks
        untuplify = map (concatMap (\(x,y) -> [x,y]))
        arrangs bs = untuplify $ concat [ arrs (div len 2) [x] (notIn [x] xs) | (x, xs) <- picks $ allPairs bs ]
