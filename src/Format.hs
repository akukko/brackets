{-# LANGUAGE OverloadedStrings #-}

module Format
    ( padLeft
    , padRight
    , combine
    , sideBySide
    ) where

import Data.List as List
import qualified Data.Text as T
    ( Text
    , zipWith
    , intercalate
    , length
    , replicate
    , strip
    )

padLeft :: Int -> T.Text -> T.Text -> T.Text
padLeft i c s
    | T.length s >= i = s
    | otherwise = padLeft i c (c <> s)

padRight :: Int -> T.Text -> T.Text -> T.Text
padRight i c s
    | T.length s >= i = s
    | otherwise = padRight i c (s <> c)

combine :: T.Text -> [T.Text] -> [T.Text] -> [T.Text]
combine endSep x y = concat $ List.zipWith (\a b -> [a, b, endSep]) x y


sideBySide :: Int -> [T.Text] -> [T.Text] -> [T.Text]
sideBySide pad f s  = do
    let fst' = map T.strip f
    let snd' = map T.strip s
    let maxA = maximum (map T.length fst')
    let (aPad, bPad) = if length fst' > length snd'
        then (map (padRight maxA " ") fst', snd' ++ repeat "")
        else (map (padRight maxA " ") (fst' ++ repeat ""), snd')
    let concatTexts a b = a <> T.replicate pad " " <> b
    List.zipWith concatTexts aPad bPad

