{-# LANGUAGE OverloadedStrings #-}

module Printing
    ( prettyMatch
    , prettyResult
    , prettyScoreboards
    , prettyFinalStandings
    , getGroupListings
    , prettyDoubleElim
    , showPrettyScoreboards
    , showPrettyTables
    , showPrettyStandings
    , showPrettyPlayoffs
    , printableSchedule
    , maxNameLength
    ) where


import Data.Types as Types
import Data.Match as Match
import Data.TeamScore as TeamScore
import Helpers
import Scoreboard
import Format

import qualified Data.Text as Text
import TextShow
import TextShow.Data.Floating ( showbFFloat )
import Data.Tuple.Extra (dupe)
import Safe.Foldable
import Data.Maybe
import Data.List.NonEmpty as NE (NonEmpty(..), toList)
import qualified Data.List as List


getScores :: NE.NonEmpty [Match] -> ([[TeamScore]], [[Match]], Int)
getScores nonEmpty = do
    let groupResults = NE.toList nonEmpty
    let longest = maxNameLength groupResults
    (buildScoreboards groupResults, groupResults, longest)

showPrettyTables :: NE.NonEmpty [Match] -> Maybe [Text.Text]
showPrettyTables nonEmpty = do
    let (scores, groupResults, longest) = getScores nonEmpty
    let prettyScores = prettyScoreboards scores
    return $ concat prettyScores

-- FIXME: Rename this to something else and make use of showPrettyTables
--       which does basically the same thing.
showPrettyScoreboards :: NE.NonEmpty [Match] -> Maybe [Text.Text]
showPrettyScoreboards nonEmpty = do
    let (scores, groupResults, longest) = getScores nonEmpty

    let groupsTitles = map (\x -> "Group " <> showt x)
                       (take (List.length scores) [1,2..] :: [Int])
    let prettyScores = zipWith (:) groupsTitles (prettyScoreboards scores)
    let prettySchedule = printableSchedule longest groupResults

    return $
        concat $
            zipWith (\ a b -> [""] ++ a ++ [""] ++ b ++ ["\n"])
            prettyScores prettySchedule

showPrettyStandings advance nonEmpty = do
    let (scores, groupResults, longest) = getScores nonEmpty
    let finalStandings = getFinalStandings scores
    case finalStandings of
        (x:xs) -> do
            let pl = maximum $ map (Text.length . fst) (addPlayoffPlacements (x:xs))
            return [ prettyFinalStandings advance pl finalStandings
                   , ""
                   ]
        _ -> Nothing


showPrettyPlayoffs nonEmpty = do
    let (scores, brackets, longest) = getScores nonEmpty
    let withLine = map (\s -> ["", s, Text.replicate (Text.length s) "=", ""])
                    (addFinalNames (List.length brackets))

    let playoffGames = printableSchedule longest brackets
    return $ concat $ zipWith (++) withLine playoffGames

addFinalNames roundAmt
    | l > 2 = reverse $ f:sf:qf:drop 3 stages
    | l > 1 = reverse $ f:sf:drop 2 stages
    | l > 0 = reverse $ f:drop 1 stages
    | otherwise = stages
    where
        stages = map (\x -> "Round of " <> showt x) teamAmounts
        teamAmounts = take roundAmt (tail $ iterate (*(2 :: Int)) 1)
        l = List.length stages
        f = "Final"
        sf = "Semifinals"
        qf = "Quarterfinals"


printableSchedule :: Int -> [[Match]] -> [[Text.Text]]
printableSchedule mt = map (map (prettyMatch mt))

prettyS pad h a (Scores hs' as') =
    let hs = padLeft scorePadAmount " " (showt hs')
        as = padRight scorePadAmount " " (showt as') in
    str' pad h a hs as
prettyS pad h a (Default Away) = str' pad h a "Forfeit" "Win by forfeit"
prettyS pad h a (Default Home) = str' pad h a "Win by forfeit" "Forfeit"


prettyResult pad h a (Tieable s) = prettyS pad h a s
prettyResult pad h a (NonTieable s Nothing) = prettyS pad h a s
-- FIXME: Pretty print the tiebreaker as well
prettyResult pad h a (NonTieable s (Just s')) = prettyS pad h a s
prettyResult pad h a (Series (s :| ss)) = Text.intercalate "\n" (prettyS pad h a s : map (prettyS pad "" "") ss) <> "\n"

prettyMatch :: Int -> Match -> Text.Text
prettyMatch pad m' = --pa 4 (showt (mid $ Match.matchID m)) <>
    case m' of
    Scheduled _ matchup ->
        let (hs, as) = dupe $ padLeft scorePadAmount " " ""
            h = rosterName $ Types.home matchup
            a = rosterName $ Types.away matchup in
            str h a hs as

    Finished m r -> prettyResult pad (rosterName $ Types.home m)
                    (rosterName $ Types.away m) r
    HomeOnly mid home -> str (rosterName home) tba tbascore tbascore
    AwayOnly mid away -> str tba (rosterName away) tbascore tbascore
    Unscheduled _ -> str tba tba tbascore tbascore
    where
        ps = padLeft scorePadAmount " "
        tba = "TBA"
        tbascore = ps ""
        str = str' pad

str' :: Int -> Text.Text -> Text.Text -> Text.Text -> Text.Text -> Text.Text
str' pad h a hs as = ph pad h <> "  " <> hs <> "  - " <> as <> "  " <> a
                                                         --pa pad a
                                                         -- this padding on right
                                                         -- doesn't seem necessary

ph :: Int -> Text.Text -> Text.Text
ph pad = padLeft pad " "

pa :: Int -> Text.Text -> Text.Text
pa pad = padRight pad " "


scorePadAmount :: Int
scorePadAmount = 4

-- Pretty printing for double elimination bracket


maxNameLength :: [[Match]] -> Int
maxNameLength = maximum . map (Text.length . longerName) . concat

prettyDoubleElim :: [[Match]] -> Text.Text
prettyDoubleElim ms = Text.intercalate "\n\n-----\n\n" $ map (Text.intercalate "\n") ms'
    where
        ms' = map (map $ prettyMatch $ maxNameLength ms) ms

-- Pretty printing the scoreboards

prettyScoreboards :: [[TeamScore]] -> [[Text.Text]]
prettyScoreboards ts =
    map (addTitles ln 1 .
         map (uncurry (prettyScore ln)) .
          addPlacements) ts
    where
        ln = longestName ts

getGroupListings :: [[TeamScore]] -> [Text.Text]
getGroupListings = map (Text.intercalate "\n" . map TeamScore.teamName)

prettyFinalStandings :: Int -> Int -> [(Int, Int, TeamScore)] -> Text.Text
prettyFinalStandings advance pl ts =
    Text.intercalate "\n" . addTitles ln pl $ withSeparator
    where
        placements = addPlayoffPlacements ts
        allTeamsInOrder = map (uncurry (prettyScore ln)) placements
        withSeparator = insertItem advance allTeamsInOrder (separator ln pl)
        ln = longestName [map thd ts]

separator :: Int -> Int -> Text.Text
separator mt pl = Text.replicate (Text.length $ getTitles mt pl) "-"

addTitles :: Int -> Int -> [Text.Text] -> [Text.Text]
addTitles mt pl ss = eqLine:ts:eqLine:ss++[eqLine]
    where
        ts = getTitles mt pl
        eqLine = Text.replicate (Text.length ts) "="

addPlacements :: [TeamScore] -> [(Text.Text, TeamScore)]
addPlacements = zip (map showt ([1..] :: [Int]))

addPlayoffPlacements :: [(Int, Int, TeamScore)] -> [(Text.Text, TeamScore)]
addPlayoffPlacements =
    map (\(group, rank, team) ->
        ("G" <> showt group <> " " <> showt rank <> ".", team))


teamNameFromScore :: TeamScore -> Text.Text
teamNameFromScore ts = rosterName (team ts)

-- Pretty printing functions for TeamScore

prettyScore :: Int -> Text.Text -> TeamScore -> Text.Text
prettyScore pad rank x =
    addSeparators [rPad (Text.length rank) rank,
                   rPad (max (Text.length rank) pad) (rosterName $ team x),
                   g x, p x, avgScore x, tot x]

getTitles :: Int -> Int -> Text.Text
getTitles mt pl = addSeparators (padTitles mt pl titles)

addSeparators :: [Text.Text] -> Text.Text
addSeparators s = "| " <> Text.intercalate sep s <> " |"

longestName :: Foldable t => t [TeamScore] -> Int
longestName ts = fromMaybe 0
                    (maximumMay $
                     map (Text.length . teamNameFromScore) $
                     concat ts)

-- mt is max team name length
-- pl is placement text length
padTitles :: Int -> Int -> [Text.Text] -> [Text.Text]
padTitles mt pl (a:b:ts) = padRight pl " " a:padRight mt " " b:ts
padTitles _  _ ts = ts

titles :: [Text.Text]
titles = ["", "Team", "Games", "Points", "Average", "Best"]

sep = " | "

gp :: Int
gp = Text.length (titles!!2)

pp :: Int
pp = Text.length (titles!!3)

avgs :: Int
avgs = Text.length (titles!!4)

bg :: Int
bg = Text.length (titles!!5)

rPad pad = padRight pad " "
g x = padLeft gp " " (showt (games x))
p x = padLeft pp " " (showt (points x))
avgScore x = padLeft avgs " " (toText $ showbFFloat (Just 2) (averageScore x))
tot x = padLeft bg " " (showt (bestGame x))
