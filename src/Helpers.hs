{-# LANGUAGE BangPatterns #-}

module Helpers where

import Data.List
import Data.List.NonEmpty (NonEmpty(..), cons)
import Data.Maybe
import System.Random.Shuffle
import Debug.Trace
import qualified Data.Set as Set
import qualified Data.Text as T
import Numeric
import Control.Monad.State

addNewlines :: [[Char]] -> [Char]
addNewlines x = "\n" ++ intercalate "\n" x ++ "\n"

showT :: Show a => a -> T.Text
showT = T.pack . show

splitHalf :: [a] -> ([a], [a])
splitHalf l = splitAt ((length l + 1) `div` 2) l

merge :: [a] -> [a] -> [a]
merge [] ys = ys
merge (x:xs) ys = x:merge ys xs

mergeGeneric :: [[a]] -> [a]
mergeGeneric = concat . transpose

-- ####
-- Math
-- ####


divide :: (Fractional a1, Integral a2, Integral a3) => a2 -> a3 -> a1
divide a b = fromIntegral a / fromIntegral b

nroot :: Floating a => a -> a -> a
nroot n x = x ** (1 / n)

average :: (Fractional a1, Real a2) => [a2] -> a1
average xs = fst $ foldl' addElement (0,0) xs
    where
      addElement (!m,!n) x = (m + (realToFrac x-m)/(n+1), n+1)

mean :: Fractional a => [a] -> a
mean y = fst $ foldl' addElement (0,0) y
    where
      addElement (!m,!n) x = (m + (x-m)/(n+1), n+1)

stddev :: (Floating a) => [a] -> a
stddev xs = sqrt $ var xs

stddevp :: (Floating a) => [a] -> a
stddevp xs = sqrt $ pvar xs

centralMoment :: (Fractional b, Integral t) => [b] -> t -> b
centralMoment xs 1 = 0
centralMoment xs r = sum (map (\x -> (x-m)^r) xs) / n
    where
      m = mean xs
      n = fromIntegral $ length xs

pvar :: (Fractional a) => [a] -> a
pvar xs = centralMoment xs 2

var :: (Fractional b) => [b] -> b
var ys = var' 0 0 0 ys / fromIntegral (length ys - 1)
    where
      var' _ _ s [] = s
      var' m n s (x:xs) = var' nm (n + 1) (s + delta * (x - nm)) xs
         where
           delta = x - m
           nm = m + delta/fromIntegral (n + 1)






numTimesFound :: Eq a => a -> [a] -> Int
numTimesFound x = length . filter (== x)

insertItem :: Int -> [a] -> a -> [a]
insertItem n xs x = let (ys,zs) = splitAt n xs in ys ++ [x] ++ zs

insertNested :: [[a]] -> Int -> a -> [[a]]
insertNested xs i e = let (ys,zs) = splitAt i xs in ys ++ [xs!!i++[e]] ++ tail zs

revFirst :: [a] -> Int -> [a]
revFirst xs i = reverse (take i xs) ++ drop i xs

numIsMore :: Eq a => [a] -> Int -> [a] -> Bool
numIsMore (m:ms) i orig
    | numTimesFound m orig >= i = True
    | otherwise = numIsMore ms i orig
numIsMore [] i orig = False

replaceAtIndex :: Int -> a -> [a] -> [a]
replaceAtIndex n item ls = a ++ (item:b) where (a, _:b) = splitAt n ls

-- | Use the given function to find a matching element from the list,
--   and replace that element with the given element.
--
--   If nothing is found, return the existing list.
replaceMatching :: (a -> Bool) -> a -> [a] -> [a]
replaceMatching cond new l = case findIndex cond l of
    Just i -> replaceAtIndex i new l
    Nothing -> l

groupByFst :: Ord b1 => [(b1, b2)] -> [(b1, [b2])]
groupByFst = groupByNth fst snd

groupBySnd :: Ord b => [(a, b)] -> [(b, [a])]
groupBySnd = groupByNth snd fst

groupByNth :: Ord a1 => (a2 -> a1) -> (a2 -> b) -> [a2] -> [(a1, [b])]
groupByNth nth other xs =
  mapMaybe f $ groupBy (\n1 n2 -> nth n1 == nth n2) $ sortOn nth xs
    where
        f (p : ps) = Just (nth p, map other (p : ps))
        f [] = Nothing

fstOfThree :: (a, b, c) -> a
fstOfThree (f,_,_) = f

sndOfThree :: (a, b, c) -> b
sndOfThree (_,s,_) = s

thd :: (a, b, c) -> c
thd (_,_,t) = t

debug printable = let t = show printable; line = replicate 20 '#' in
    trace $ line ++ " Debug print " ++ line ++ "\n\n" ++ t ++ "\n\n" ++ line ++ " End of debug print " ++ line ++ "\n\n"

(|>) :: NonEmpty a -> a -> NonEmpty a
(|>) xs x = xs <> pure x

(|:) :: [a] -> a -> NonEmpty a
(|:) xs x = foldr cons (pure x) xs

chunks :: Int -> [a] -> [[a]]
chunks _ [] = []
chunks n xs
    | n < 1 = []
    | otherwise = let (ys, zs) = splitAt n xs in  ys : chunks n zs

combinations :: Int -> [a] -> [[a]]
combinations 0 _  = [ [] ]
combinations n xs = [ y:ys | y:xs' <- tails xs
                           , ys <- combinations (n-1) xs']

allPairs :: [b] -> [(b, b)]
allPairs xs = [(y, z) | (y, ys) <- picks xs, z <- ys]

picks :: [x] -> [(x, [x])]
picks [] = []
picks (x : xs) = (x, xs) : [(y, ys) | (y, ys) <- picks xs]


nubOrd :: Ord a => [a] -> [a]
nubOrd = go Set.empty
    where
        go s (x:xs)
            | x `Set.member` s = go s xs
            | otherwise = x : go (Set.insert x s) xs
        go _ _ = []

prettyError s = error $ "\n" ++ s ++"\n" ++ replicate (length s) '^' ++ "\n"

-- |This takes in a list of teams, an integer defining the size of a basket and an
-- integer defining the number of baskets in use.
-- Then the teams are shuffled so that teams within baskets can be shuffled amongst
-- each other. Once enough baskets are shuffle amongst each other, the rest of the teams
-- are shuffled amongst each other.
getShuffled ts 0 _ = return ts
getShuffled ts _ 0 = shuffleM ts
getShuffled [] _ _= return []
getShuffled ts basketSize basketAmount = do
    let first = take basketSize ts
    shuffled <- shuffleM first
    end <- getShuffled (drop basketSize ts) basketSize (basketAmount-1)
    return (shuffled ++ end)

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s = wordsWhenRec p s "" []

wordsWhenRec :: (Char -> Bool) -> String -> String -> [String] -> [String]
wordsWhenRec p (s:ss) r t
    | p s = wordsWhenRec p ss "" (t++[r])
    | otherwise = wordsWhenRec p ss (r++[s]) t
wordsWhenRec p [] r t = t++[r]

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c

-- | Converts an uncurried function to a curried function.
curry3 :: ((a, b, c) -> d) -> a -> b -> c -> d
curry3 f a b c = f (a,b,c)

uncurry4 :: (t1 -> t2 -> t3 -> t4 -> t5) -> (t1, t2, t3, t4) -> t5
uncurry4 f (a, b, c, d) = f a b c d

curry4 :: ((a, b, c, d) -> t) -> a -> b -> c -> d -> t
curry4 f a b c d = f (a,b,c,d)


uncurry5 :: (t1 -> t2 -> t3 -> t4 -> t5 -> t6) -> (t1, t2, t3, t4, t5) -> t6
uncurry5 f (a, b, c, d, e) = f a b c d e

curry5 :: ((a, b, c, d, e) -> t) -> a -> b -> c -> d -> e -> t
curry5 f a b c d e = f (a,b,c,d,e)

roundFloat :: RealFloat a => Int -> a -> T.Text
roundFloat numOfDecimals floatNum =
    T.pack $ showFFloat (Just numOfDecimals) floatNum ""


mods :: MonadState b m => (b -> b) -> m b
mods f = do { x <- get; put (f x); return (f x) }
