# Note: This is just a minimal example. For proper usage, see the README.

{ nixpkgs ? (import ./nixpkgs.nix).pkgsMusl, compiler ? "ghc924", strip ? true }:


let

  pkgs = nixpkgs.pkgsMusl;

  brackets =
    { mkDerivation
    , stdenv
    , base
    , acid-state
    , aeson
    , aeson-casing
    , bytestring-encoding
    , concurrent-extra
    , safe
    , scalpel
    , text-show
    , unicode-show
    , utf8-string
    , blaze-builder
    , blaze-html
    , blaze-markup
    , bytestring
    , containers
    , directory
    , extra
    , http-client
    , http-types
    , hvega
    , mtl
    , pretty-show
    , safecopy
    , text
    , time
    , wai
    , warp
    , criterion
    , clay
    , html-parse
    , tasty
    , tasty-hunit
    , tf-random
    , wai-extra
    , fuzzyfind
    , string-qq
    , http-client-tls
    , base64
    , bcrypt
    , generic-arbitrary
    , matrix
    , quickcheck-instances
    , random-shuffle
    , wai-app-static
    }:
    mkDerivation {
      pname = "release";
      version = "0.1.0.0";
      src = pkgs.lib.sourceByRegex ./. [
        ".*\.cabal$"
        "^Setup.hs$"
        ".*\.hs"
        "server"
        "generator"
        "src"
        "src/.*"
        "test"
        "test/.*"
        "LICENSE"
        "static"
        "static/.*"
      ];
      isLibrary = false;
      isExecutable = true;
      enableSharedExecutables = false;
      enableSharedLibraries = false;
      # Skip running test suite
      doCheck = false;
      executableHaskellDepends = [
        base
        acid-state
        aeson
        aeson-casing
        blaze-builder
        blaze-html
        blaze-markup
        bytestring-encoding
        concurrent-extra
        safe
        scalpel
        text-show
        unicode-show
        utf8-string
        bytestring
        containers
        directory
        extra
        http-client
        http-types
        hvega
        mtl
        pretty-show
        safecopy
        text
        time
        wai
        warp
        criterion
        tasty
        tasty-hunit
        tf-random
        wai-extra
        fuzzyfind
        string-qq
        clay
        html-parse
        http-client-tls
        base64
        bcrypt
        generic-arbitrary
        matrix
        quickcheck-instances
        random-shuffle
        wai-app-static
      ];
      license = pkgs.lib.licenses.bsd3;
      configureFlags = [
        "--ghc-option=-optl=-static"
        "--extra-lib-dirs=${pkgs.gmp6.override { withStatic = true; }}/lib"
        "--extra-lib-dirs=${pkgs.zlib.static}/lib"
        "--extra-lib-dirs=${pkgs.libffi.overrideAttrs (old: { dontDisableStatic = true; })}/lib"
      ] ++ pkgs.lib.optionals (!strip) [
        "--disable-executable-stripping"
      ];
    };

  normalHaskellPackages = pkgs.haskell.packages.${compiler};

  haskellPackages = with pkgs.haskell.lib; normalHaskellPackages.override {
    overrides = self: hsuper: {
      fuzzyfind = nixpkgs.haskell.lib.doJailbreak hsuper.fuzzyfind;
      html-parse = nixpkgs.haskell.lib.doJailbreak hsuper.html-parse;
      clay = nixpkgs.haskell.lib.doJailbreak hsuper.clay;
      servant-foreign = nixpkgs.haskell.lib.doJailbreak hsuper.servant-foreign;
      servant-multipart = nixpkgs.haskell.lib.doJailbreak hsuper.servant-multipart;

      bytestring-encoding = nixpkgs.haskell.lib.dontCheck hsuper.bytestring-encoding;
      generic-data = nixpkgs.haskell.lib.dontCheck hsuper.generic-data;

      # # Dependencies we need to patch
      # hpc-coveralls = appendPatch super.hpc-coveralls (builtins.fetchurl https://github.com/guillaume-nargeot/hpc-coveralls/pull/73/commits/344217f513b7adfb9037f73026f5d928be98d07f.patch);
    };
  };

  drv = haskellPackages.callPackage brackets { };

in
if pkgs.lib.inNixShell then drv else drv
