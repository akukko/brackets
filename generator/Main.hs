{-# LANGUAGE OverloadedStrings #-}


module Main where

import Scheduling
import Scoreboard
import Config
import Data.Types as Types
import Data.Vault
import Data.Match as Match
import Printing
import Helpers
import Groups
import Data.Result


import Interactive
import Brackets
import Format
import IO.ReadTeams
import IO.ReadResults
import IO.Write
import IO.Locations
import Data.Competition

import Prelude ( IO
               , ($)
               , otherwise
               , Bool(..)
               , (<$>)
               , not
               , readFile
               , (+)
               , (*)
               , (&&)
               , (<>)
               , (.)
               , concat
               , Int
               , null
               , error
               , (==)
               , flip
               , foldl
               )

import System.Environment
import Data.List as List
import qualified Data.List.NonEmpty as NE
import Data.Ord
import Control.Monad
import Control.Exception
import Control.DeepSeq
import Text.Show.Unicode
import System.IO ( print
                 , FilePath
                 )
import System.Directory
import Data.Maybe

import Data.HashMap.Strict as HashMap (HashMap, insert, empty, lookup)
import qualified Data.ByteString.Lazy as BS
import Data.Text as Text
    ( pack
    , unpack
    , Text
    , lines
    , concat
    , intercalate
    , length
    , replicate
    , null
    )

import Data.Text.IO as TextIO ( putStrLn )
import Data.Binary
import TextShow ( showt, printT )
import Text.Show.Pretty


import Debug.Trace
import Config (seedingBasketSize, seedingBasketsAmount)



main :: IO ()
main = do
    createDirectoryIfMissing False userDataDir

    v <- maybeGenerateNew

    file <- readFile groupStageFile

    let groupResults = readResults file

    forM_ (showPrettyScoreboards =<< NE.nonEmpty groupResults) (mapM_ TextIO.putStrLn)
    forM_ ((showPrettyStandings advance) =<< NE.nonEmpty groupResults) (mapM_ TextIO.putStrLn)
    let cid = relatedCompetition $ Match.matchID (head $ head groupResults)

    let comp = case HashMap.lookup cid (competitions v) of
            Just c -> c { groups = groupResults }
            Nothing -> do
                let cid = CompetitionID Config.competitionName Config.competitionSeason
                let comp = Competition cid groupResults []
                comp

    playoffsExist <- doesFileExist playoffFile
    c' <-
        if playoffsExist
        then do
            playoffs <- readFile playoffFile
            evaluate (force playoffs)
            let results = readResults playoffs
            return $ updatePlayoffs $ addPlayoffsToCompetition results comp
        else
            return $ updatePlayoffs comp

    let brackets = playoffs c'

    unless (List.null brackets) $ writeSchedule playoffFile brackets

    forM_ (showPrettyPlayoffs =<< NE.nonEmpty brackets) (mapM_ TextIO.putStrLn)
    saveCompetition c'

    putStrLn ""


maybeGenerateNew :: IO Vault
maybeGenerateNew = do
    args <- getArgs
    let newGen = not $ Prelude.null args

    v <- getVault
    if newGen then do
        let teamPath = head args
        ts <- readFile teamPath

        let teams = readTeams ts
        let validNames = not $ any Text.null teams
        evaluate (force teams)

        -- FIXME: Fix discrepancy between the seeding configs
        -- and the TwelveFields logic. Unify them into a single
        -- configuration flag if possible.
        shuffledTeams <- getShuffled teams seedingBasketSize seedingBasketsAmount
        let groups = getGroups shuffledTeams groupFormat

        doSomething v groups

    else
        return v

getID oldID = oldID + 1

doSomething :: Vault -> [[Text]] -> IO Vault
doSomething v grouppedTeams = do
    let cid = CompetitionID Config.competitionName Config.competitionSeason

    let (vaultWithTeams, ids) = addTeamsToVault v (List.concat grouppedTeams)

    -- FIXME: Get real TeamIDs from the vault

    let rosters = map (map ((\tid t -> Roster (TeamID (getID tid)) t []) 0)) grouppedTeams
    --let groups = getGroups rosters maxTeams -- FIXME: Move group creation to earlier phase and do it conditionally only if groups are not provided.

    let schedule = getSchedule rosters balancing (MatchID cid 0) -- FIXME: Using zero might actually be correct here.
    let comp = Competition cid schedule []
    let vaultWithComps = addCompetitionToVault comp vaultWithTeams

    writeTeams teamsFile grouppedTeams
    writeSchedule groupStageFile schedule


    saveVault vaultWithComps

    return vaultWithComps


addFinalNames stages
    | l > 2 = reverse $ f:sf:qf:drop 3 stages
    | l > 1 = reverse $ f:sf:drop 2 stages
    | l > 0 = reverse $ f:drop 1 stages
    | otherwise = stages
    where
        l = List.length stages
        f = "Final"
        sf = "Semifinals"
        qf = "Quarterfinals"
