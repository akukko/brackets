{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module GeneratorTests
    ( runGeneratorTests ) where


import Data.Types as Types
import Data.Competition
import Scoreboard
import Data.TeamScore as TeamScore
import Brackets
import Scheduling
import Config
import Groups
import Printing
import qualified Data.Match as Match
import Data.Matchup
import Helpers
import Balance

import Arbitrary
import TestHelpers



import qualified Data.Text as T
import qualified Data.Text.IO as TIO (putStrLn)
import Test.QuickCheck
import Debug.Trace
import Text.Show.Pretty
import TextShow
import qualified Data.List.NonEmpty as NE
import Control.Monad
import Data.Maybe
import Data.List
import Test.HUnit
import System.Console.Pretty

runGeneratorTests = do
    testHeadline "\nTesting properties with QuickCheck:"

    propTests

    testHeadline "Testing generator with HUnit:"


    let hUnitTests = [ ("refreshing playoffs", refreshWorks)
                     , ("12 field schedule", testTwelveFieldGroupSchedule)
                     , ("12 field groups", testTwelveFieldGroupAmounts)
                     , ("generating tournament", basicTournament)
                     , ("tiebreakers", tiebreakers)
                     , ("standings between 4 groups", fourGroups)
                     , ("basic group split", basicSplitting)
                     ]
    runTests hUnitTests


-- #####################
-- Helpers for the tests
-- #####################

{-
These results are built so that:

A is the sole first place holder.

B, C and D are tied in points, but the order will be determined by
peer to peer matches so that B > C > D.

E, F and G are tied in points and peer to peer matches, but E and F
both have better overall average than G. E and F are tied in overall average,
but E has better average in the peer to peer matches.

H and I are tied in points, peer to peer points, overall average and
peer to peer average, but H has better best game than I.

-}
tiebreakerResults :: [(T.Text, T.Text, Int, Int)]
tiebreakerResults = [ ("1", "2", 1, 0)
                    , ("1", "4", 1, 0)
                    , ("1", "3", 1, 0)
                    , ("1", "5", 1, 0)
                    , ("1", "6", 1, 0)

                    , ("2", "4", 1, 0)
                    , ("2", "3", 1, 0)
                    , ("3", "4", 1, 0)
                    , ("2", "5", 11, 10)
                    , ("3", "6", 11, 10)
                    , ("3", "7", 11, 0)
                    , ("4", "5", 11, 5)
                    , ("4", "6", 22, 15)
                    , ("4", "7", 11, 0)

                    , ("5", "6", 8, -4)
                    , ("6", "7", 3, 1)
                    , ("7", "5", 3, 1)
                    , ("5", "6", 1, 3)
                    , ("6", "7", 1, 3)
                    , ("7", "5", 1, 3)

                    , ("8", "9", 4, 0)  -- The peer to peer matches between
                    , ("8", "9", 0, 4)  -- H and I yield the same average
                    , ("1", "8", 20, -10) -- H has better best game, while having
                    , ("1", "8", 20, 10)  -- the same overall average as I
                    , ("1", "9", 20, 0)
                    , ("1", "9", 20, 0)
                    ]


generateResults :: [(T.Text, T.Text, Int, Int)] -> [Match]
generateResults rs = do
    let result a b as bs = Finished (Matchup testMid a b) (Tieable $ Scores as bs)
    Prelude.map (\(h, a, hs, as) -> result (roster h) (roster a) hs as) rs

fourTeamRoundRobin = do
    let cid = CompetitionID "TestComp" 2021
    let ts = ["A", "B", "C", "D"]
    let comp = generateTournament cid SingleRoundRobin ts
    comp

singleRRMatchAmt ((m:ms):gs) = length ms + singleRRMatchAmt (ms:gs)
singleRRMatchAmt ([]:gs) = singleRRMatchAmt gs
singleRRMatchAmt [] = 0

-- #####
-- Tests
-- #####



propTests = do
    let wait = 4 * 10^6

    let groups fmt predicate teams = within wait $
            let ts = nub teams :: [String]
                gs = getGroups ts fmt
                teamAmt = length ts in
            predicate gs teamAmt

    let checkTwelve gs teamAmt = if teamAmt > 3
            then divide teamAmt (length gs) >= 3
            else teamAmt <= 0 || not (null gs)

    let checkThreeGroups gs teamAmt = teamAmt <= 3 || length gs == 3

    let checkSRR [ts] teamAmt = length ts == teamAmt
        checkSRR _ _ = False


    quickCheck (groups TwelveFields checkTwelve)
    quickCheck (groups ThreeGroups checkThreeGroups)
    quickCheck (groups SingleRoundRobin checkSRR)


    let structure (x:xs) (y:ys) = length x == length y && structure xs ys
        structure [] [] = True
        structure _  _  = False

    let balance teams b = within wait $
            -- Filter duplicate names, because those would generate
            -- matches where a team plays against itself.
            let uniqueTeams = nub teams
                gs = getGroups uniqueTeams SingleRoundRobin
                matchups = getMatchups gs b testMid in
            structure matchups (balanceMatches b matchups)

    -- This takes pretty long so limit the amount of test cases
    quickCheck (withMaxSuccess 10 balance)

    let scheduleForGroup teams mid = within wait $
            let group = nub teams
                matchups = getScheduleForGroup group mid in
            length matchups == singleRRMatchAmt [group]

    quickCheck scheduleForGroup

    -- This is a very wonky way of doing the same thing as splitToNGroups.
    -- However, it would seem to produce the same results so it can be used
    -- to test splitToNGroups.
    let testSplitToGroups :: [a] -> Int -> Int -> [[a]] -> [[a]]
        testSplitToGroups (x:xs) amt cur groups -- amt is group amount
            | length groups < amt - 1 = testSplitToGroups xs amt 0 (groups++[[x]])
            | length groups < amt = testSplitToGroups (revFirst xs amt) amt 0 (groups++[[x]])
            | cur == amt - 1 && modTwo = testSplitToGroups xs amt 0 newList
            | cur == amt - 1 = testSplitToGroups (revFirst xs amt) amt 0 newList
            | otherwise = testSplitToGroups xs amt (cur + 1) newList
            where
                newList = insertNested groups cur x
                modTwo = even (length (head groups))
        testSplitToGroups [] _ _ groups = groups

    let splitGroups teams (Positive groupAmount) = do
            let types = teams :: [String]
            let groups = splitToNGroups groupAmount teams
            let correct = testSplitToGroups teams groupAmount 0 []

            correct == groups

    quickCheck $ forAll (listOf genSafeString) splitGroups

tiebreakers = do
    let assertCorrectOrder [scores] =
            Prelude.map TeamScore.team scores ==
                map (roster . (T.pack . show)) [1..9]
        assertCorrectOrder _ = False

    let scoreboards = buildScoreboards [generateResults tiebreakerResults]

    let printable = map (T.intercalate "\n") $ prettyScoreboards scoreboards
    assertBool "Faulty tiebreaker logic" $ assertCorrectOrder scoreboards

refreshWorks = do
    let c = fourTeamRoundRobin

    -- Get the first group game for later
    let firstGroupGame = fromJust $ getNextMatch c

    -- Add scores for all groups scores so playoffs get generated
    let c' = addScore (Scores 1 0) firstGroupGame c
    let c1 = foldl (flip (tryUpdateNextMatch . Scores 1)) c' [1..5]

    assertBool
        "Playoffs were not generated after all group game results are in" $
        not $ null (playoffs c1)

    -- Remove one group score so playoffs are invalidated
    let c2 = tryDeletePreviousScore c1

    assertBool "Playoffs were not reset after group game was deleted" $
        null (playoffs c2)

    -- Add back the last match so playoffs are regenerated
    let c3 = tryUpdateNextMatch (Scores 0 0) c2

    assertBool "Playoffs were not regenerated after group game was re-added" $
        not $ null (playoffs c3)

    -- Add semifinal results so that the final game is scheduled, then modify
    -- a group game that doesn't affect the semifinal pairings.
    let c4 = foldl (flip (tryUpdateNextMatch . Scores 1)) c3 [2..3]
    let c5 = addScore (Scores 2 0) firstGroupGame c4


    assertEqual "Playoffs were changed after insignificant group match update"
        (playoffs c4) (playoffs c5)

    -- This changes both semifinal pairings
    let c6 = addScore (Scores 0 1) firstGroupGame c5

    assertBool "Playoffs were not reset after significant group match update"
        (playoffs c5 /= playoffs c6 &&
         not (any Match.isFinished $ head $ playoffs c6))

    -- This changes only the latter semifinal pairing (B vs. C -> C vs. B)
    -- FIXME: Should the score still remain if teams only swap order in match?
    let c6' = addScore (Scores 0 0) firstGroupGame c5

    assertBool "Playoffs were not partially reset after group match update"
        (playoffs c5 /= playoffs c6' &&
         Match.isFinished (head $ head $ playoffs c6') &&
         not (Match.isFinished (head (playoffs c6')!!1)) &&
         Match.isHomeOnly (head (playoffs c6'!!1)))

basicTournament = do
    let comp = fourTeamRoundRobin
    assertBool "had correct group sizes" $
        length (groups comp) == 1 && length (head $ groups comp) == 6

basicSplitting = do
    let teams = [1,2,3,4,5,6,7,8,9]
    let groups = splitToNGroups 4 teams
    let correct = [[1,8,9], [2,7], [3,6], [4,5]]
    assertEqual "Groups were not split correctly" correct groups


    -- Here the last team is placed in the first group.
    -- More info in splitToNGroups documentation.
    let teams = [1,2,3,4,5,6,7]
    let groups = splitToNGroups 2 teams
    let correct = [[1,4,5,7], [2, 3, 6]]

    assertEqual "Groups were not split correctly" correct groups

testTwelveFieldGroupAmounts = do
    let options =
            [1, 2, 6, 7, 12, 13, 18, 24, 25, 28, 31, 35, 36, 39, 42, 43, 46, 48, 100]
    let correctGroupAmounts =
            [0, 1, 1, 2,  2,  3,  3,  4,  5,  6, 7,  8,  8,   9, 10, 11, 12, 12,  0]

    let teams = Prelude.map (\x -> take x (iterate (1+) 1)) options
    let groups = Prelude.map (`getGroups` TwelveFields) teams

    let groupAmounts = zip options $ Prelude.map length groups
    mapM_ (uncurry $ assertEqual "Wrong group amount for 12 field logic") $
        zip (zip options correctGroupAmounts) groupAmounts

testTwelveFieldGroupSchedule = do
    let teams = Prelude.map (roster . T.pack . show) [1..35]

    let groups = getGroups teams TwelveFields
    let schedule = getSchedule groups ConcurrentTwo testMid

    let assertDifferentTeams (Scheduled _ b) (Scheduled _ a) = differentTeams a b
        assertDifferentTeams _ _ = False

    let assertConcurrent ((a:b:ms):gs) =
            assertDifferentTeams a b && assertConcurrent (ms:gs)
        assertConcurrent ([]:gs) = assertConcurrent gs
        assertConcurrent _   = True

    assertBool "Schedule didn't have concurrent matches" $
        assertConcurrent schedule

fourGroups = do
    let fmt g x y a b = do
            let groupLetter 1 = "A"
                groupLetter 2 = "B"
                groupLetter 3 = "C"
                groupLetter 4 = "D"
                groupLetter _ = "G" <> showt g

            let groupText g txt = groupLetter g <> txt
            let getScore score group = score - group
            (groupText g x, groupText g y, getScore a g, getScore b g)

    let grp g = map (uncurry4 (fmt g)) tiebreakerResults
    let groupResults = map generateResults $ grp 1 : [grp 2] ++ [grp 3] ++ [grp 4]

    let scores = buildScoreboards groupResults
    let sortedScores = getFinalStandings scores
    let standings = map (\(a,b,_) -> (a,b)) sortedScores

    let correctOrder = concatMap (\standing -> map (, standing) [1..4]) [1..9]

    assertEqual "Four group standings were in wrong order" correctOrder standings

    unless True $
        TIO.putStrLn $ "\n" <> prettyFinalStandings 16 0 sortedScores



-- #########
-- Old tests
-- #########

firstRound :: Bool
firstRound = do
    let scoreboards = buildScoreboards [generateResults tiebreakerResults]
    let brackets = getSingleEliminationBracket (map (take 8) scoreboards) testMid
    unpackAndReturn "firstRound" "" True


testCalculateGroupAmount :: Bool
testCalculateGroupAmount = do
    let options = [(16, 4), (32, 4), (4, 4), (3, 4)]
    let amounts = Prelude.map (uncurry calculateGroupAmount) options
    trace (ppShow amounts)
        True

testBuildEmptyScoreboards :: Bool
testBuildEmptyScoreboards = do
    let teams = Prelude.map (roster . T.pack . show) [1..35]

    let groups = getGroups teams TwelveFields
    let schedule = getSchedule groups ConcurrentTwo testMid
    let scores = buildEmptyScoreboards schedule

    trace (ppShow scores) True



testShufflingTeams :: IO ()
testShufflingTeams = do
    let teams = map show [1..32]

    shuffled <- getShuffled teams 4 2 :: IO [String]
    let groups = getGroups shuffled TwelveFields

    putStrLn (ppShow groups)

assertTwoTeamsInScores :: [[a]] -> Bool
assertTwoTeamsInScores [[_, _]] = True
assertTwoTeamsInScores _ = False


printing _ _ = ""
{-
printing (Seeded (NonTieable ((Scheduled (Matchup {home = h, away = a}), Nothing)))) txt = txt <> rosterName h <> " - " <> rosterName a <> "\n"
printing (WinnersOf b1 b2) txt = printWinnersOf b1 b2 "" <> txt


printWinnersOf (Seeded (NonTieable (Scheduled (Matchup {home = h1, away = a1}), Nothing))) (Seeded (NonTieable (Scheduled (Matchup {home = h2, away = a2}), Nothing))) txt =
        rosterName h1 <> "/" <> rosterName a1 <> " - " <> rosterName h2 <> "/" <> rosterName a2 <> "\n" <> txt
printWinnersOf (WinnersOf a1 a2) (WinnersOf b1 b2) txt = printWinnersOf a1 a2 "" <> printWinnersOf b1 b2 "" <> txt
printWinnersOf _ _ txt = txt

-}
