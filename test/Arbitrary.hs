-- |

module Arbitrary where

import Data.Types
import Config
import TestHelpers


import Data.Text as T
import Test.QuickCheck
import Test.QuickCheck.Arbitrary

instance Arbitrary Balancing where
    arbitrary = elements [Sparse, ConcurrentTwo, NoBalancing]


instance Arbitrary GroupStageFormat where
    arbitrary = elements [TwelveFields, SingleRoundRobin, ThreeGroups]

instance Arbitrary PlayoffFormat where
    arbitrary = elements [SingleElimination]

instance Arbitrary Roster where
    arbitrary = roster <$> arbitrary

instance Arbitrary T.Text where
    arbitrary = T.pack <$> arbitrary

instance Arbitrary CompetitionID where
    arbitrary = do
        Positive year <- arbitrary
        flip CompetitionID year <$> arbitrary

instance Arbitrary MatchID where
    arbitrary = do
        Positive mid <- arbitrary
        flip MatchID mid <$> arbitrary

instance Arbitrary Matchup where
    arbitrary = do
        mid <- arbitrary
        home <- arbitrary
        away <- arbitrary
        return $ Matchup mid home away

instance Arbitrary SafeString where
    arbitrary = SafeString <$> genSafeString
