{-# LANGUAGE OverloadedStrings #-}

module TestHelpers
    ( runTests
    , ppAndReturn
    , unpackAndReturn
    , roster
    , testCid
    , testMid
    , oneFourMemberGroup
    , uniques
    , testWithTimeEstimate
    , testHeadline
    , genSafeString
    , SafeString(..)
    ) where


import Data.Types as Types
import Data.Vault

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Text as T
import Text.Show.Pretty
import Debug.Trace ( trace )
import qualified Data.Text.IO as TIO (putStrLn)
import Data.Time.Clock
import Test.QuickCheck (Gen(..), listOf, elements, Arbitrary(..))
import Data.List (nub)

import System.Console.Pretty

import Test.HUnit

testHeadline txt = putStrLn (style Bold $ color Yellow ("\n" <> txt))

runTests hUnitTests = runTestTT $ TestList $
    map (\(label, test) -> TestLabel label (TestCase test)) hUnitTests


genSafeChar :: Gen Char
genSafeChar = elements ['a'..'z']

genSafeString :: Gen [Char]
genSafeString = listOf genSafeChar

newtype SafeString = SafeString { unwrapSafeString :: String }
    deriving Show



unpackAndReturn tn x = printer tn (T.unpack x)
ppAndReturn tn x = printer tn (ppShow x)

printer :: [Char] -> [Char] -> Bool -> Bool
printer testname testOutput = trace ("Output from test: " ++
    testname ++ "\n" ++
    testOutput)

roster :: T.Text -> Roster
roster name = Roster (TeamID 1) name []

testCid :: CompetitionID
testCid = CompetitionID "Test" 2020

testMid :: MatchID
testMid = MatchID testCid 0

uniques :: Eq a => Gen a -> Gen [a]
uniques gen = nub <$> listOf gen

oneFourMemberGroup :: String -> String -> String -> String -> Bool
oneFourMemberGroup t1 t2 t3 t4 = do
    let ts = Prelude.map T.pack [t1, t2, t3, t4]
    let (vaultWithTeams, teamIDs) = addTeamsToVault emptyVault ts

    let fourTeamsExist = HashMap.size (teams vaultWithTeams) == 4

    trace (ppShow vaultWithTeams) fourTeamsExist


testWithTimeEstimate paramN avgTime testable = do
    t1 <- getCurrentTime

    let expectedSeconds = avgTime * fromIntegral paramN
    let hourS = 3600
    let minS  = 60

    putStrLn $              "Parameter amount:   " ++ show paramN
    putStrLn $              "Started run at:     " ++ show t1
    let estimatePrintout est
            | est > hourS = "Expected runtime:   " ++ show (expectedSeconds / hourS) ++ " hours"
            | est > minS  = "Expected runtime:   " ++ show (expectedSeconds / minS) ++ " minutes"
            | otherwise   = "Expected runtime:   " ++ show expectedSeconds ++ " seconds"

    putStrLn $ estimatePrintout expectedSeconds
    putStrLn $              "Expected to finish: " ++ show (addUTCTime (fromIntegral (round expectedSeconds)) t1)
    putStrLn ""

    text <- testable

    TIO.putStrLn text

    t2 <- getCurrentTime

    let diff =  diffUTCTime t2 t1

    let timePrintout d
            | d > hourS = "\nFinding parameters took: " ++ show (nominalDiffTimeToSeconds (d / hourS)) ++ " hours\n"
            | d > minS  = "\nFinding parameters took: " ++ show (nominalDiffTimeToSeconds (d / minS)) ++ " minutes\n"
            | otherwise = "\nFinding parameters took: " ++ show (nominalDiffTimeToSeconds d) ++ " seconds\n"

    putStrLn $ timePrintout diff
    putStrLn $ "Finding parameters took on average " ++ show (diff / fromIntegral paramN) ++ "\n"
