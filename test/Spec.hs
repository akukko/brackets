{-# LANGUAGE OverloadedStrings #-}

module Main where


import GeneratorTests
import TestHelpers
import LeagueTests
import WebScrapingTests

import Test.HUnit

import Control.Monad

main :: IO Counts
main = run True

run :: Bool -> IO Counts
run loud = do

    runGeneratorTests

    runWebScrapingTests loud
    runLeagueTests loud
