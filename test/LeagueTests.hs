{-# LANGUAGE OverloadedStrings #-}


module LeagueTests
    ( runLeagueTests
    ) where

import TestHelpers
import Scheduling
import League
import IO.FetchLeagueResults
import Scoreboard
import Printing

import qualified Data.Text as T
import Criterion.Main
import Criterion.Measurement
import Criterion.Measurement.Types (measTime)
import Data.Time.Clock
import Data.Time
import Text.Show.Pretty
import GHC.Generics (Generic1(to1))
import qualified Data.Text.IO as TIO
import Test.HUnit
import Control.Monad


amt = 11

runLeagueTests loud = do
    let leagueTests =
            [ -- ("test league standings", leagueStandings loud) -- needs files, bad
             --, ("test league scheduling", testLeagueScheduling loud)
            ]

    testHeadline "Testing league scheduling with HUnit"
    runTests leagueTests

testLeagueScheduling loud = do
    let pairs = buildPairings amt
    let x = buildSchedules 2 2 pairs

    let days = januaryDays

    sFile <- readFile "userData/schedules"

    let schedules = read sFile :: [(String, [Day])]
    let exceptions = map snd schedules
    let fitting = map (fittingDays days) exceptions
    let fitAll = fitsAll fitting

    when loud $ pPrint fitAll

    assertEqual "League scheduling produced wrong amount of games" (length x) 24


benchLeagueScheduling = do
    let iter = 1
    t1 <- getCurrentTime
    measure (whnf (buildSchedules 2 2) (buildPairings amt)) iter
    t2 <- getCurrentTime
    let diff = diffUTCTime t2 t1

    let diffS = "Scheduling with " ++ show amt ++ ": " ++ show (diff / fromIntegral iter) ++ "\n"
    appendFile "runtime.prof" diffS

    return True

leagueStandings loud = do
    -- FIXME: Make this work without reading files
    rFile <- readFile "userData/results"

    let results = readLeagueResults rFile
    let scoreboards = buildScoreboards [results]

    let printable = map (T.intercalate "\n") $ prettyScoreboards scoreboards

    when loud $ TIO.putStrLn $ "\n" <> T.intercalate "\n" printable

