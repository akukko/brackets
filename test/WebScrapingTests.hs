{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}


module WebScrapingTests
    ( runWebScrapingTests
    ) where


import Web.Front
import Web.Fantasy
import Web.Difference
import Helpers
import Format
import TestHelpers


import qualified Data.Text.IO as TIO (putStrLn)
import Text.Show.Pretty
import qualified Data.Text as T
import Data.List
import Data.Ord (Down(..))
import TextShow ( showt, printT )
import Control.Monad
import Data.Time.Clock
import System.Directory

runWebScrapingTests loud = do
    let fantasyPrintouts =
            [ frontPage
            , testPointsPerThrow
            --, testDifferentParameters
            --, testTotalPoints

            ]

    when loud $ sequence_ fantasyPrintouts

testBonus = do
    let sets = [4, 0, -5, -10, -20, -40] :: [Int]
    let lefts = [60, 40, 20] :: [Double]

    let combined = map (,sets) lefts

    let addNewLine = concatMap (++[""])

    let mapped = addNewLine $ map (map showt . (\(left, sets) -> map (tfklBonus (-80) left) sets)) combined
    let txts = addNewLine $ map (\(left, sets) -> map (\s -> showt left <> ", " <> padLeft 4 " " (showt s)) sets) combined
    let zipped = zipWith (\ a b -> a <> "  " <> b) txts mapped


    putStrLn ""
    TIO.putStrLn $ T.intercalate "\n" zipped

    --pPrint zipped

testDifferentPlusPoints = do
    let pts = [ (22, 80)
              , (20, 80)
              , (16, 80)
              , (12, 80)
              , (6, 80)
              , (2, 80)
              , (20, 40)
              , (16, 40)
              , (12, 40)
              , (6, 40)
              , (2, 40)
              , (12, 12)
              , (10, 10)
              , (8, 8)
              , (6, 12)
              , (6, 6)
              , (4, 4)
              , (2, 4)
              , (2, 6)
              , (2, 2)
              ]

    putStrLn ""


    let points f = map (uncurry f)

    putStrLn ""
    let tampere = points tfklPlus pts
    let oulu    = points ofklPlus pts

    --pPrint (zip tampere oulu)
    --putStrLn ""
    putStrLn ""


    let percentiles x = (\y -> map (\a -> a / sum y) y) (map (\b -> b - minimum x) x)

    let toTxt (a, b) = padLeft 7 " " (roundFloat 4 (a * 100)) <> " " <> padLeft 7 " " (roundFloat 4 (b * 100))


    let both = zip pts $ points tfklPlus pts
    let fantasyPoints = map (\((a,b), c) ->
            "remove " <> padLeft 3 " " ((T.pack . show) a) <>
            " /" <> padLeft 3 " " ((T.pack . show . round) b) <>
            " = " <> padLeft 6 " " (roundFloat 1 c)) both

    let perce = zipWith (curry toTxt) (percentiles tampere) (percentiles oulu)

    let zipped = zipWith (\ a b -> a <> "     " <> b)
            ("tfkl points" <> T.replicate 13 " ":fantasyPoints) ("tre" <> T.replicate 6 " " <>"oulu":perce)

    TIO.putStrLn $ T.intercalate "\n" zipped

bestEverFileName = "userData/allTimeBest"
bestFileName = "userData/best"

testDifferentParameters = do
    let smallNums = [0.001, 0.01, 0.1, 0.5, 1, 1.5, 3, 5, 10]



    let ns = [0.45, 0.6 .. 1.1]
    let as = [0.35, 0.36 .. 0.5]
    let bs = [1.55, 1.6 .. 1.65]
    let cs = [1.5, 1.51 .. 1.65]

    let hs = [0]--0 : [0.01, 0.08 .. 0.5]

    let paramN = length ns * length as * length bs * length cs * length hs

    let avgTime = average
                    [ 0.001129401612
                    , 0.001115124832
                    , 0.001130209397
                    ]

    let season = All

    stats <- getStats season
    let text = testDifParams ns as bs cs hs stats season
    testWithTimeEstimate paramN avgTime text


testDifParams ns as bs cs hs stats season = do
    let pmsF x = (concat . concat . concat) (concatMap (\n -> map (\a-> map (\b -> map (\c -> map (n, a, b, c,) x) cs) bs) as) ns)
    let pms = pmsF hs
    let params = map (uncurry5 pointParams) pms
    let names x  = map (\(n,_,_,_,_,_,_,_) -> n) (getPlayerListing x)
    let fromParams = map (names . (`getTotalPlayerPointsFromData` stats))

    let plrList = fromParams params
    let zipped = zip pms plrList

    let filt = (filter (not . T.null) <$> T.splitOn "\n") . T.pack
    correct <- case season of
        Regular -> filt <$> readFile "userData/correctRegular"
        Playoffs -> filt <$> readFile "userData/correctPlayoffs"
        _ -> return []

    let guesses = generateGuesses zipped correct

    let roundF2 = roundFloat 2
    let roundF3 = roundFloat 3
    let roundF4 = roundFloat 4
    let texts n a b c h avgE cor max plr =
            let pad5 = padLeft 5 " "
                pad4 = padLeft 4 " " in
                    padLeft 7 " " n <> pad5 a  <> pad5 b <> pad5 c <> pad5 h <> padLeft 6 " " avgE <>
                    pad4 cor <> pad4 max <> " " <> plr

    let textsF n a b c h avgE cor max plr =
            texts (roundF3 n) (roundF2 a) (roundF2 b) (roundF2 c) (roundF2 h) (roundF3 avgE) (showt cor) (showt cor) plr



    let theBestTitles = texts "n" "a" "b" "c" "h" "avgE" "cor" "max" "plr"
    let theBest = map (\(BetResult (n, a, b, c, h) avgE (cor, _) (max, plr)) -> textsF n a b c h avgE cor max plr) (take 13 guesses)

    writeFile "userData/guesses.txt" (ppShow (take 100 guesses))

    let bestParams = head guesses
    writeFile bestFileName (show bestParams)

    beExists <- doesFileExist bestEverFileName
    bestEver <- if not beExists
        then do
            writeFile bestEverFileName (show bestParams)
            return bestParams
        else
            read <$> readFile bestEverFileName :: IO (BetResult (Double, Double, Double, Double, Double))

    let leftPrint = (theBestTitles : theBest) ++ concatMap (T.splitOn "\n") ["", "Best ever: ", "",  T.pack $ ppShow bestEver, "", "Best of current run: ", "", T.pack $ ppShow bestParams, ""]

    when (avgError bestEver > avgError bestParams) $
        writeFile bestEverFileName (show bestParams)

    let (BetResult (n, a, b, c, h) _ _ _) = bestParams
    let best = pointParams n a b c h

    rightPrint <- getPage season best

    let bestPlrs = head $ fromParams [best]
    let dif = "dif" : "---" : map (padLeft 3 " " . showt . fst) (generateDifList bestPlrs correct)
    let ofklPlrs = head $ fromParams [ofkl]
    let ofklDif = "ofkl" : "----" : map (padLeft 4 " " . showt . fst) (generateDifList bestPlrs ofklPlrs)


    return $ T.intercalate "\n" $
        sideBySide 4 leftPrint $ sideBySide 1 rightPrint $ sideBySide 1 dif ofklDif

testTotalPoints = do
    params <- read <$> readFile bestFileName :: IO (BetResult (Double, Double, Double, Double, Double))

    let (BetResult (n, a, b, c, h) _ _ _) = params
    let best = pointParams n a b c h
    r <- getPage Regular best
    p <- getPage LatestPlayoffs best

    putStrLn ""
    TIO.putStrLn $ T.intercalate "\n" (sideBySide 3 r p)


    putStrLn ""


frontPage = do
    txt <- getFrontPage
    TIO.putStrLn txt

testEssoDLPoints = do
    ptsPerThrow <- getPointsPerThrow Regular tfkl
    let dlEsso = ptsPerThrow !! 1

    let comparisons =
            [ (juliaTwo, joriSix)
            , (juliaTwo, eliasFive)
            , (juliaThree, juliaTwo)
            , (joriSix, eliasFive)
            , (kukkoSeven, joriSix)
            ]

    let pts g (a, b) = (a g, b g)

    let throws = map (pts dlEsso) comparisons
    let points = map (uncurry more) throws
    let result = zip throws points


    --pPrint dlEsso
    pPrint result

    --return $ not (any snd result)


testDLPlayoffGames = do
    ptsPerThrow <- getPointsPerThrow Playoffs tfkl
    let dlGames = [ptsPerThrow !! 6, ptsPerThrow !! 7]

    pPrint dlGames


testPointsPerThrow = do
    total <- getPointsPerThrow Playoffs tfkl
    pPrint total



testPetterson = do
    putStrLn ""
    ptsPerThrow <- (concatMap (uncurry (++)) <$> getPointsPerThrow Regular tfkl) :: IO [(T.Text, FantasyStats)]

    let who x (p,_) = p == x
    let petterson = filter (who "Olli Petterson") ptsPerThrow
    let niemi = filter (who "Ville Niemi") ptsPerThrow
    let niemi = filter (who "Sakari Rautalin") ptsPerThrow

    putStrLn ""

testIiro = do
    ptsPerThrow <- (concatMap (uncurry (++)) <$> getPointsPerThrow LatestPlayoffs tfkl) :: IO [(T.Text, FantasyStats)]

    let who x (p,_) = p == x
    let player = filter (who "Iiro Pulska") ptsPerThrow
    putStrLn ""

testPlrPointsAgainstLiiga plr throws = do
    putStrLn ""

    let points f = map (uncurry f)
    let all = points ofklPlus throws

    ptsPerThrow <- (concatMap (uncurry (++)) <$> getPointsPerThrow Regular ofkl) :: IO [(T.Text, FantasyStats)]

    let who x (p,_) = p == x
    let player = filter (who plr) ptsPerThrow

    let p = map (\(_, FantasyStats pts _ _ _ _ _) -> pts) player

    pPrint p
    pPrint $ sum p
    pPrint $ sum p / 8

    putStrLn "\n----\n"

    pPrint all
    pPrint (sum all)
    pPrint (sum all / 8)


koveroThrows =
    [ (0, 56)
    , (2, 25)
    ]

mjattiThrows =
    [ (4, 80)
    , (8, 76)
    , (10, 44)
    , (12, 34)

    , (10, 80)
    , (6, 70)
    , (10, 40)
    , (4, 30)
    ]

antikainenThrows =
    [ (4, 72)
    , (12, 68)
    , (6, 24)
    , (6, 18)

    , (0, 78)
    , (8, 78)
    , (12, 40)
    , (9, 28)
    ]


testMjattiPoints = testPlrPointsAgainstLiiga "Matti Eteläperä" mjattiThrows
testKoveroPoints = testPlrPoints koveroThrows
testAntikainenPoints = testPlrPoints antikainenThrows



testPlrPoints throws = do
    putStrLn ""

    let points f = map (uncurry f)
    let all = points ofklPlus throws

    let totalPts = sum all
    pPrint all
    pPrint totalPts
    pPrint $ totalPts / 8


more (_, pts) (_, pts') = pts > pts'




juliaThree (dl, _) = dl !! 28
juliaTwo (dl, _)   = dl !! 29
joriSix (_, esso)  = esso !! 30
eliasFive (dl, _) = dl !! 24
kukkoSeven (dl, _) = dl !! 2
